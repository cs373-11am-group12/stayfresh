.DEFAULT_GOAL := all
SHELL := bash

# install dependencies for react
start:
	npm install
	make dockerNode
	npm start

# run docker
dockerBack:
	docker login
	docker pull wgunawan/stayfresh-backend-dev
	docker run --rm -i -t -v $(PWD)/backend:/stayfresh/backend -w /stayfresh/backend wgunawan/stayfresh-backend-dev

dockerFront:
	docker login
	docker pull mariazgu/stayfresh-frontend
	docker run --rm -i -t -v $(PWD)/frontend:/stayfresh/frontend -w /stayfresh/frontend mariazgu/stayfresh-frontend

# upload files to StayFresh repo after git add and git commit
push:
	git stash
	git checkout Development
	git pull
	git stash pop
	git push
	git status

# auto format all Python files in the backend
format:
	black ./backend/*.py
