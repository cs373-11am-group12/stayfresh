import sys
import time
import pytest

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Remote
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# based off of https://gitlab.com/10am-group-8/adopt-a-pet/-/blob/main/front-end/selenium/gui_test.py 
# and https://gitlab.com/Nathaniel-Nemenzo/getthatbread/-/blob/main/frontend/getthatbread/selenium/test_gui.py

driver = None
wait = None
local = False

url = "https://www.stay-fresh.me/"
PATH = "./frontend/gui_tests/chromedriver"

def setup_module(module):
    print("beginning setup for test_gui module")

    # allow gitlab ci/cd to run selenium tests
    global driver, wait
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("window-size=1200x600")
    if local:
        driver = webdriver.Chrome(service = Service(ChromeDriverManager().install()), chrome_options = chrome_options)
    else:
        driver = Remote(
            "http://selenium__standalone-chrome:4444/wd/hub",
            desired_capabilities=chrome_options.to_capabilities(),
        )
    driver.get(url)
    wait = WebDriverWait(driver, 20)
    return driver

def teardown_module(module):
    print("tearing down test_gui module")
    driver.quit()

# 1
def test_title():
    print("starting test_title")
    assert driver.title


# 2
def test_navbar_about():
    print("starting test_navbar_about")
    about = driver.find_element(By.LINK_TEXT, "About")
    about.click()
    assert driver.current_url == "https://www.stay-fresh.me/about"


# 3
def test_navbar_products():
    print("starting test_navbar_products")
    products = driver.find_element(By.LINK_TEXT, "Products")
    products.click()
    assert driver.current_url == "https://www.stay-fresh.me/products?page=1"


# 4
def test_navbar_recipes():
    print("starting test_navbar_recipes")
    recipes = driver.find_element(By.LINK_TEXT, "Recipes")
    recipes.click()
    assert driver.current_url == "https://www.stay-fresh.me/recipes?page=1"


# 5
def test_navbar_locations():
    print("starting test_navbar_locations")
    locations = driver.find_element(By.LINK_TEXT, "Locations")
    locations.click()
    assert driver.current_url == "https://www.stay-fresh.me/locations?page=1"


# 6
def test_navbar_logo():
    print("starting test_navbar_logo")
    logo = driver.find_element(By.ID, "nav-link-logo")
    logo.click()
    assert driver.current_url == "https://www.stay-fresh.me/"


# 7
def test_navbar_about_back():
    print("starting test_navbar_about_back")
    about = driver.find_element(By.LINK_TEXT, "About")
    about.click()
    assert driver.current_url == "https://www.stay-fresh.me/about"
    driver.back()
    assert driver.current_url == "https://www.stay-fresh.me/"


# 8
def test_navbar_products_back():
    print("starting test_navbar_products_back")
    products = driver.find_element(By.LINK_TEXT, "Products")
    products.click()
    assert driver.current_url == "https://www.stay-fresh.me/products?page=1"
    driver.back()
    assert driver.current_url == "https://www.stay-fresh.me/"


# 9
def test_navbar_recipes_back():
    print("starting test_navbar_recipes_back")
    recipes = driver.find_element(By.LINK_TEXT, "Recipes")
    recipes.click()
    assert driver.current_url == "https://www.stay-fresh.me/recipes?page=1"
    driver.back()
    assert driver.current_url == "https://www.stay-fresh.me/"

# 10
def test_navbar_locations_back():
    print("starting test_navbar_recipes_back")
    locations = driver.find_element(By.LINK_TEXT, "Locations")
    locations.click()
    assert driver.current_url == "https://www.stay-fresh.me/locations?page=1"
    driver.back()
    assert driver.current_url == "https://www.stay-fresh.me/"
