import React from 'react';
import Pagination from '@mui/material/Pagination';
import './pages/style.css';

const MyPagination = props => {
    const pages = [];

    let lastpage = Math.ceil(props.totalInstances / props.instancesPer);
    for (let i = 1; i <= lastpage; ++i)
        pages.push(i);

    // avoid negative bounds or overflows
    lastpage = lastpage === 0 ? 1 : lastpage;
    let next = parseInt(props.page) + 1;
    let nextPage = next < lastpage ? next : lastpage;
    let prev = props.page - 1;
    let prevPage = prev > 0 ? prev : 1;

    return (
        <div className={"pagination center"}>
            <Pagination
                className={"center"}
                count={lastpage}
                variant="outlined"
                shape="rounded"
                color="success"
                page={props.page}
                onChange={(e, page) => props.paginate(page)}
        />
        </div>
        
    );
};

export default MyPagination;
