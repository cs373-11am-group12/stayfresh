import React, { useState } from 'react';
import {
    FormControl,
    InputGroup,
    Button,
} from 'react-bootstrap';
import {
    StringParam,
    NumberParam,
    useQueryParams,
    withDefault,
} from 'use-query-params';
import { useNavigate, useLocation } from 'react-router-dom';
import { Search } from 'react-bootstrap-icons';
import '../pages/style.css';
import Filter from './Filter';
import Sort from './Sort';

const SearchBar = props => {
    const [input, setInput] = useState("");
    const [filter, setFilter] = useState("Filter");
    const [sort, setSort] = useState("Sort");

    let location = useLocation();
    let navigate = useNavigate();

    const filters = props.filters;
    const filterOptions = props.filterOptions;
    const sorts = props.sorts;

    const [params, setParams] = useQueryParams({
        page: withDefault(NumberParam, 1),
        q: withDefault(StringParam, ""),
    });

    const selectFilter = e => {setFilter(e)};
    const selectSort = e => {setSort(e)};

    const enter = e => {
        console.log(location.pathname);
        if (e.charCode === 13 || e._reactName === "onClick")
            if (location.pathname.includes('products') ||
                location.pathname.includes('recipes') ||
                location.pathname.includes('locations'))
                setParams({ page: 1, q: input })
            else 
                navigate(`/search?q=${input}`);
    }

    return (
        <div className="">
            {<Filter
                filters={filters}
                filterOptions={filterOptions}
                selectFilter={selectFilter}
            />}
            <div className={"center search"}>
                <InputGroup className="mb-3">
                    {sorts.length > 0 ?
                    <Sort 
                        sorts={sorts}
                        selectSort={selectSort}
                    /> : <div></div>
                    }
                    <FormControl 
                        placeholder="Search..."
                        onKeyPress={enter}
                        onChange={e => setInput(e.target.value.toLowerCase())}
                        value={input}
                    />
                    <Button
                        variant="success"
                        onClick={enter}
                    >
                        <Search />
                    </Button>
                </InputGroup>
            </div>
        </div>
    );
}

export default SearchBar;
