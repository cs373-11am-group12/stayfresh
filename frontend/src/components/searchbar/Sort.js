import React, { useState } from 'react';
import {
    Dropdown,
    DropdownButton,
    ButtonGroup
} from 'react-bootstrap';
import { StringParam, useQueryParam } from 'use-query-params';

const Sort = props => {
    const [sort, setSort] = useQueryParam("sort", StringParam);

    return (
        <DropdownButton
            onSelect={props.selectSort}
            as={ButtonGroup}
            variant="success"
            title="Sort"
        >
            {props.sorts.map(attribute => (
                <Dropdown.Item 
                    key={attribute}
                    // eventKey={attribute}
                    onClick={() => {
                        let sort_query = `${attribute}`;
                        if (sort_query.includes("descending")) {
                            sort_query = sort_query.replace(" (descending)","").toLowerCase();
                            sort_query = '-' + sort_query;
                        } else 
                            sort_query = sort_query.replace(" (ascending)","").toLowerCase();
                        setSort(sort_query);
                    }}
                    value={attribute}
                >
                    {attribute}
                </Dropdown.Item>
            ))}
            <Dropdown.Divider />
            <Dropdown.Item
                key="Sort"
                eventKey="Sort"
                value="Sort"
            >
                Reset
            </Dropdown.Item>
        </DropdownButton>
    );
};

export default Sort;
