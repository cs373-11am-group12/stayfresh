import React, { useState } from 'react';
import {
    Dropdown,
    DropdownButton,
    ButtonGroup,
    InputGroup,
    FormControl,
    Button
} from 'react-bootstrap';
import '../pages/style.css';
import { StringParam, useQueryParams, withDefault } from 'use-query-params';

const Filter = props => {
    let attributes = new Map();
    props.filters.forEach(attribute => 
        attributes.set(attribute, withDefault(StringParam, "")));

    const [params, setParams] = useQueryParams(attributes);
    const [min, setMin] = useState(0);
    const [max, setMax] = useState(0);

    return (
        <>
            <div className={`search filter`}>
                {props.filters.map((attribute, index) => {
                        const filterOptions = props.filterOptions[index];
                        return (
                            <DropdownButton
                                key={index}
                                className="filter_item"
                                as={ButtonGroup}
                                variant="success"
                                title={`Filter by ${attribute}`}
                            >
                                {filterOptions.length > 0 
                                ? 
                                filterOptions.map((option, index) => {
                                    let att = attribute.toString().toLowerCase();
                                    let opt = option.toString().toLowerCase();
                                    return (
                                        <Dropdown.Item
                                            key={index}
                                            onClick={() => setParams({ page: 1, q: params.q, [att] : opt })}
                                        >
                                            {`${option}`}
                                        </Dropdown.Item>);
                                })
                                :
                                <>
                                    <InputGroup>
                                        <FormControl
                                            placeholder="min"
                                            className="filter_item"
                                            onChange={e => setMin(e.target.value)}
                                        />
                                        <div className="filter_item">
                                            -
                                        </div>
                                        <FormControl
                                            placeholder="max"
                                            className="filter_item"
                                            onChange={e => setMax(e.target.value)}
                                        />
                                        
                                    </InputGroup>
                                    <Button
                                        className="center"
                                        variant="success"
                                        onClick={() => {
                                            let range = `${min}-${max}` ;
                                            setParams({ page: 1, q: params.q, [attribute.toString().toLowerCase()]: [range]})}
                                        }
                                    >
                                        Apply
                                    </Button>
                                </>
                                
                                }
                            </DropdownButton>
                        )
                   })
                }
                </div>
        </>
    );
};

export default Filter;
