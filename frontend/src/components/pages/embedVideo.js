import React from "react";

// function to embed the YouTube video containing our presentation
// embeds video using sharable link in src field
const EmbedVideo = () => {
    
    // variable to set style for section title
    const titleStyle = {
        color: "green",
        padding: "10px",
        fontFamily: "Impact",
        textAlign: "center",
        justifyContent: "center",
        fontSize: "1.7em"
    };
    
      return (
        <div className = "center">
            <h1 style = {titleStyle}> StayFresh Project Presentation </h1>
            <iframe 
                width="560"
                height="315" 
                src="https://www.youtube.com/embed/Xfcb4DUv0PU" 
                frameborder="0" 
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                allowfullscreen>
            </iframe>
        </div>
    );
}

export default EmbedVideo;
