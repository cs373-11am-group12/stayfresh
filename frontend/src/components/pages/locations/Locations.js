import React, { useEffect, useState } from 'react';
import axios from 'axios';
import RingLoader from "react-spinners/RingLoader";
import {
    CardImg,
    Card,
    Row,
    Col
} from 'react-bootstrap';
import {
    StringParam,
    NumberParam,
    useQueryParams,
    withDefault,
} from 'use-query-params';
import Pagination from '../../Pagination';
import SearchBar from '../../searchbar/SearchBar';
import Highlighter from "react-highlight-words";

const Locations = () => {
    const endpoint = 'https://api.stay-fresh.me/locations';
    const [locations, setLocations] = useState([]);
    const [loading, setLoading] = useState(false);
    const [numLocations] = useState(10);

    const [params, setParams] = useQueryParams({
        page: withDefault(NumberParam, 1),
        q: withDefault(StringParam, ""),
        sort: withDefault(StringParam, ""),
        products: withDefault(StringParam, ""),
        schedule: withDefault(StringParam, ""),
        address: withDefault(StringParam, ""),
        zip: withDefault(StringParam, "")
    });

    useEffect(() => {
        setLoading(true);
        let URLParams = new URLSearchParams();
        URLParams.append("q", params.q);
        if (params.sort)
            URLParams.append("sort", params.sort);
        if (params.products)
            URLParams.append("Products", params.products);
        if (params.schedule)
            URLParams.append("schedule", params.schedule);
        if (params.address)
            URLParams.append("address", params.address);
        if (params.zip)
            URLParams.append("lzip", params.zip);
        axios
            .get(`${endpoint}?${URLParams}`)
            .then((response) => {
                setLocations(response.data['page']);
                setLoading(false);
            })
            .catch(e => console.log(e));
      }, [params]);

    const lastIndex = params.page * numLocations;
    const firstIndex = lastIndex - numLocations;
    const currentLocations = locations.slice(firstIndex, lastIndex);

    const paginate = page => setParams({ ...params, page: page });

    const override = 
    `
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%)
    `;
    const Loading = () => 
        <RingLoader 
            color={'#7ED381'}
            loading={loading}
            css={override}
            size={150}
        />;

    const ShowLocations = () => {
        return (
            <>
                <Row xs={1} md={5} className="g-4">
                    {currentLocations.map(location => (
                        <Col key={location.id}>
                            <Card width = "20%" src={location}>
                                <Card.Link href={`/locations/${location.id}`}> 
                                <CardImg width = "20%" src={location.picture} alt="Market Picture" />
                                <Card.Body>
                                <Card.Title><Highlighter autoEscape={true} textToHighlight={location.location_name} searchWords={params.q.split(/[%20 \s,;/]+/)}/></Card.Title>
                                <Card.Title>Products</Card.Title>
                                <Card.Text>
                                    <Highlighter autoEscape={true} textToHighlight={location.location_products} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    <br/>
                                    <Card.Title>Schedule:</Card.Title>
                                    <Card.Text>
                                    <Highlighter autoEscape={true} textToHighlight={location.location_schedule.replace(";<br> <br> <br>","")} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                    <Card.Title>Address</Card.Title>
                                    <Card.Text>
                                    <Highlighter autoEscape={true} textToHighlight={location.location_address} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                </Card.Text>
                                </Card.Body>
                                </Card.Link>
                            </Card>
                        </Col>
                    ))}
                </Row>
                <Pagination 
                    instancesPer={numLocations}
                    totalInstances={locations.length}
                    paginate={paginate}
                    page={params.page}
                />
            </>
        );
    };

    return (
        <>
            <SearchBar
                    filters={["Products", "Schedule", "Address", "Zip"]}
                    filterOptions = {[
                        ["Baked goods", "Cheese and/or dairy products", "Crafts and/or woodworking items", "Cut flowers", "Eggs", 
                            "Fresh fruit and vegetables", "Fresh and/or dried herbs", "Honey", "Canned or preserved fruits", "vegetables", "jams", 
                            "jellies", "preserves", "salsas", "pickles", "dried fruit",  "Meat", "Nuts", "Poultry", "Prepared foods (for immediate consumption)", "Soap and/or body care products"],
                        ["2022", "2021", "2020", "2019", "2018", "2017"],
                        ["Texas", "New York", "Connecticut", "Rhode Island", "New Jersey", "California"],
                        ["78701", "78745", "78723", "78759", "78613", "78660", "78669", "78665", "78621", "78634", "78602", "78628", "78130", "78259", "78209", "78214", "78228", "75234", "75218",
                            "76248", "76201", "75009", "76063", "76240", "75119", "76033", "75020", "75103", "75773", "75494", "75701", "76701", "11575", "10573", "10580", "10543", "11355"]
                    ]}
                    sorts={["Name", "Schedule", "Address"]}
                />
            {loading ? <Loading /> : <ShowLocations />}
        </>
    );
}

export default Locations;
