import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import RingLoader from "react-spinners/RingLoader";
import { CardImg } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import CardGroup from 'react-bootstrap/CardGroup';

const categories = {
    "fresh fruit and vegetables" : ["pea", "potato", "eggplant", "broccoli", "bell pepper", "onion", "tomato",
            "lentil", "chili pepper", "garlic", "ginger", "olive", "spinach", "mushroom", "kale",
            "basil", "kidney bean", "carrots", "celery", "jalapeno", "lettuce", "cucumber", "red onion",
            "yellow onion", "shallot", "avacado", "horseradish", "cabbage", "fava beans", "plum tomato",
            "chile", "beetroot", "radish", "bok choy", "bean sprout",
            "lemon", "orange", "mango", "apple", "plum", "blackberry", "raspberry", "lime", "coconut",
            "pear", "banana", "kiwi", "stawberry", "watermelon", "guava"],
    "canned or preserved fruits, vegetables, jams, jellies, preserves, salsas, pickles, dried fruit, etc." : ["raisin"],
    "fish and/or seafood" : ["mussels", "fish", "shrimp", "salmon", "anchovy", "prawn", "squid", "haddock", "cod"],
    "meat" : ["chicken", "sausage", "beef", "pork", "duck", "bacon", "ham", "prosciutto", "pancetta"],
    "cheese and/or dairy products" : ["sour cream", "paneer", "milk", "cheese", "yogurt", "cheddar cheese", "gouda cheese", "mozzerlla cheese", "feta"],
    "honey" : ["honey"],
    "wine, beer, hard cider" : ["wine"],
    "eggs" : ["egg"],
    "nuts" : ["walnut"],
    "maple syrup and/or maple products" : ["maple syrup"],
    "herbs" : ["coriander", "cinnamon","mint" ,"thyme", "bay leaf", "cumin", "parsley", "rosemary", "clove",
        "saffron", "paprika", "dill"],
    "baked goods" : ["bread"],
    "prepared foods (for immediate consumption)" : ["peanut butter", "mayonnaise", "mustard", "pasta", "fish stock", "chicken stock", "ketchup"]
};

const Location = () => {
    const { id } = useParams();
    const endpoint = `https://api.stay-fresh.me/locations`;
    const [location, setLocation] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() =>{
        setLoading(true);
        axios
            .get(endpoint)
            .then(res => {
                setLocation(res.data['page'][parseInt(id) - 1]);
                setLoading(false);
            })
            .catch(e => console.log(e));
    },[endpoint, id]);

    const override = 
    `
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%)
    `;
    const Loading = () => 
        <RingLoader 
            color={'#7ED381'}
            loading={loading}
            css={override}
            size={150}
        />;

    const ShowLocation = () => {
        let url = location.location_map;
        if(url) {
            url = /(.*)%20/.exec(url)[0];
            url = url.replace("http://maps.google.com/?","https://www.google.com/maps/embed/v1/place?key=AIzaSyAahoQXXzXmrfKtM4CWVqMrc2yNPIAH5Ts&");
        }
        let schedule;
        if(location.location_schedule){
            schedule = location.location_schedule.replace(";<br> <br> <br>","");
        }
        let categoryList = ( location.location_products !== undefined) ? location.location_products.split('; ') : [];

        return (
                <Card className="text-center"style={{width:"50%"}}>
                    <Card.Header >Location</Card.Header>
                    <Card.Body>
                        <h1>{location.location_name}</h1>
                        <CardImg width = "20%" src="location.location_picture" alt="Market Picture" />
                        <Card.Title>Products</Card.Title>
                        <Card.Text>
                                {location.location_products}
                            <br/>
                            <Card.Title>Schedule:</Card.Title>
                            <Card.Text>
                                {schedule} <br/>
                            </Card.Text>
                            <Card.Title>Address</Card.Title>
                            <Card.Text>
                                {location.location_address} <br/>
                            </Card.Text>
                            <Card.Title>Google Maps</Card.Title>
                            <div className="google-map-code">
                                <iframe src={url} width="500" height="450" frameborder="0" style={{border:0}} allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>
                        </Card.Text>
                    </Card.Body>
                <CardGroup>
                    <Card>
                        <Card.Header >Products</Card.Header>
                        <Card.Body>
                        <Card.Text>
                            {categoryList.map(category => (
                                    categories[category.toLowerCase()] !== undefined ? 
                                    <div>
                                        {(categories[category.toLowerCase()]).map(product => (
                                            <Card.Link href={`/products/?page=1&q=${product}`}> {product} <br/> </Card.Link>
                                        ))}
                                    </div> : <div></div>
                             ))}
                        </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Header>Recipes</Card.Header>
                        <Card.Body>
                            {categoryList.map(category => (
                                        categories[category.toLowerCase()] !== undefined ? 
                                        <div>
                                            {(categories[category.toLowerCase()]).map(product => (
                                                <Card.Link href={`/recipes/?ingredients=${product}&page=1`}> Recipes with {product} <br/> </Card.Link>
                                            ))}
                                        </div> : <div></div>
                                ))}
                        </Card.Body>
                    </Card>
                </CardGroup>
            <Card.Footer className="text-muted"><Card.Link href="/locations?page=1">View more Locations</Card.Link></Card.Footer>
            </Card>
        );
    }

    return (
        <div aria-label='location' style={{display: 'flex', justifyContent: 'center', padding: "3em 0em",}}>
           {loading ? <Loading /> : <ShowLocation />}
        </div>)
}

export default Location;
