import React, {useState, useEffect} from 'react';
import RingLoader from "react-spinners/RingLoader";
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Link, Outlet } from 'react-router-dom';
import SearchBar from '../searchbar/SearchBar';
import EmbedVideo from "./embedVideo"
import './style.css'

function Home () {
    const [loading, setLoading] = useState(false)

    const headingStyle = {
        color: "green",
        padding: "10px",
        fontFamily: "Impact",
        textAlign: "center",
        justifyContent: "center",
        fontSize: "2em"
      };

    const textStyle = {
        color: "black",
        padding: "10px",
        fontFamily: "Times New Roman",
        textAlign: "left",
        justifyContent: "center",
        fontSize: "1.2em"
    };

    const override = 
    `
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%)
    `;

    useEffect(() => {
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
        }, 1500)
    }, [])

    var intro = 
        <div>
            <h1 style={headingStyle}> Welcome to StayFresh!</h1>
            <SearchBar filters={[]} filterOptions={[]} sorts={[]} />
            <p style={textStyle}>
                StayFresh is a website that helps individuals find FRESH, 
                local produce and meat from farmer's markets. These food items 
                generally are organic and don't have any added chemicals, leading 
                to a healthier lifestyle. The website provides data about produce, 
                helps people find the nearest farmer's market, and even helps them 
                with finding a recipe that uses the ingredients that they will or 
                have already bought. Ready to live healthier? Use the navigation bar 
                to access information about produce, locations of farmers' markets,
                recipes to make with your recently bought food, or check statistics about
                this page!
            </p>
        </div>
        
    var body = 
        <Row className='row'>
        <Col>
        <Link to="/products?page=1" className='card-link'>
        <Card className='card'>
            <Card.Img
                className='card-image'
                variant='top'
                src='https://freshworld.us/assets/images/main/produce_meat_seafood_02.jpg'
            />
                <Card.Body>
                    <Card.Title>Products</Card.Title>
                    <Card.Text>
                    Check out the page to obtain more information on various fruits, vegetables, and meats!
                    Learn about the nutritional content, best time to eat, and how to best make different foods.
                    </Card.Text>
                </Card.Body>
        </Card>
        </Link>
        </Col>
        <Col>
        <Link to="/locations?page=1" className='card-link'>
        <Card className='card'>
            <Card.Img
                className='card-image'
                variant='top'
                src='https://mediaproxy.salon.com/width/1200/https://media.salon.com/2021/08/farmers-market-produce-0812211.jpg'
            />
                <Card.Body>
                    <Card.Title>Locations</Card.Title>
                    <Card.Text>
                    This page will let you know where to find a farmers' market near you! By shopping and eating local,
                    you can support the farmers in your community and eat food that is home grown, which is not filled
                    with artificial preservitives.
                    </Card.Text>
                </Card.Body>
        </Card>
        </Link>  
        </Col>
        <Col>
        <Link to="recipes?page=1" className='card-link'>
        <Card className='card'>
            <Card.Img
                className='card-image'
                variant='top'
                src='https://images.immediate.co.uk/production/volatile/sites/30/2020/08/meatball-black-bean-chilli-2-bf7378b.jpg?quality=90&resize=400,363'
            />
                <Card.Body>
                    <Card.Title>Recipes</Card.Title>
                    <Card.Text>
                    What will you do with the food you just purchased? Check out some tasty recipes that you can make
                    with the ingredients you just purchased at your local farmers' market!
                    </Card.Text>
                </Card.Body>
        </Card>
        </Link>
        </Col>
        </Row>

    return (
        loading ?
        <RingLoader
            color={'#7ED381'}
            loading={loading}
            css={override}
            size={150}
        />
        :
        <>
            <React.Fragment>
                {intro}
                {body}
                <EmbedVideo/>
            </React.Fragment>
            <Outlet />
        </>
    );
}

export default Home;
