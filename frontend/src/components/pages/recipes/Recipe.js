import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import RingLoader from "react-spinners/RingLoader";
import { CardImg, CardGroup} from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

const categories = {
    "fresh fruit and vegetables" : ["pea", "potato", "eggplant", "broccoli", "bell pepper", "onion", "tomato",
            "lentil", "chili pepper", "garlic", "ginger", "olive", "spinach", "mushroom", "kale",
            "basil", "kidney bean", "carrots", "celery", "jalapeno", "lettuce", "cucumber", "red onion",
            "yellow onion", "shallot", "avacado", "horseradish", "cabbage", "fava beans", "plum tomato",
            "chile", "beetroot", "radish", "bok choy", "bean sprout",
            "lemon", "orange", "mango", "apple", "plum", "blackberry", "raspberry", "lime", "coconut",
            "pear", "banana", "kiwi", "stawberry", "watermelon", "guava"],
    "Canned or preserved fruits, vegetables, jams, jellies, preserves, salsas, pickles, dried fruit, etc." : ["raisin"],
    "Fish and/or seafood" : ["mussels", "fish", "shrimp", "salmon", "anchovy", "prawn", "squid", "haddock", "cod"],
    "meat" : ["chicken", "sausage", "beef", "pork", "duck", "bacon", "ham", "prosciutto", "pancetta"],
    "Cheese and/or dairy products" : ["sour cream", "paneer", "milk", "cheese", "yogurt", "cheddar cheese", "gouda cheese", "mozzerlla cheese", "feta"],
    "honey" : ["honey"],
    "Wine, beer, hard cider" : ["wine"],
    "eggs" : ["egg"],
    "nuts" : ["walnut"],
    "Maple syrup and/or maple products" : ["maple syrup"],
    "herbs" : ["coriander", "cinnamon","mint" ,"thyme", "bay leaf", "cumin", "parsley", "rosemary", "clove",
        "saffron", "paprika", "dill"],
    "baked goods" : ["bread"],
    "Prepared foods (for immediate consumption)" : ["peanut butter", "mayonnaise", "mustard", "pasta", "fish stock", "chicken stock", "ketchup"]
};

const Recipe = () => {
    const { id } = useParams();
    const endpoint = `https://api.stay-fresh.me/recipes`;
    const [recipe, setRecipe] = useState([]);
    const [loading, setLoading] = useState(false);

    const productsEnd = `https://api.stay-fresh.me/products`;
    const [allProducts, setAllProducts] = useState([]);
    const locationsEnd = `https://api.stay-fresh.me/locations`;
    const [allLocations, setAllLocations] = useState([]);
    const [products, setProducts] = useState([]);
    const [category, setCategory] = useState([]);

    useEffect(() =>{
        setLoading(true);
        axios
            .get(endpoint)
            .then(res => {
                console.log(res);
                setRecipe(res.data['page'][parseInt(id) - 1]);
                let recipe_ingredients = `${res.data['page'][parseInt(id) - 1].recipe_ingredients}`;
                let prod = [];
                let cat = [];
                for(var c in categories) {
                    let arr = categories[c];
                    for(var p in arr){
                        if(recipe_ingredients.includes(arr[p])){
                            prod.push(arr[p]);
                            if(!cat.includes(c)){
                                cat.push(c);
                            }
                        }
                    }
                }
                setCategory(cat);
                setProducts(prod);
                setLoading(false);
            })
        axios
            .get(productsEnd)
            .then(res => {
                setAllProducts(res.data['page']);
            })
        axios
            .get(locationsEnd)
            .then(res => {
                setAllLocations(res.data['page']);
            })
            .catch(e => console.log(e));
    },[endpoint, id]);

    const override = 
    `
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%)
    `;
    const Loading = () => 
        <RingLoader 
            color={'#7ED381'}
            loading={loading}
            css={override}
            size={150}
        />;

    const ShowRecipe = () => {
        console.log(category);

        return (
            <div style={{display: 'flex', justifyContent: 'center', padding: "3em 0em",}}>
                <Card className="text-center"style={{width:"50%"}}>
                    <Card.Header >Recipe</Card.Header>
                    <Card.Body>
                        <h1>{recipe.recipe_name}</h1>
                        <CardImg width = "20%" src={recipe.recipe_picture} alt="recipe image" />
                        <Card.Text>
                            <b>Meal Type:</b><br/> {recipe.recipe_meal}
                        </Card.Text>
                        <Card.Text>
                            <b>Cuisine Type:</b><br/> {recipe.recipe_type}
                        </Card.Text>
                        <Card.Text>
                            <b>Health labels:</b><br/> {recipe.recipe_health}<br/>
                        </Card.Text>
                        <Card.Text>
                            <b>Cautions:</b><br/> 
                            {recipe.recipe_cautions}
                        </Card.Text>
                        <Card.Text>
                            <b>Nutrients:</b><br/>
                            Total Calories: {Number.parseFloat(recipe.recipe_calories).toFixed(2)}<br/>
                            Total Fat: {Number.parseFloat(recipe.recipe_fat).toFixed(2)}<br/>
                            Protein: {Number.parseFloat(recipe.recipe_protein).toFixed(2)}<br/>
                            Sugar: {Number.parseFloat(recipe.recipe_sugar).toFixed(2)}<br/>
                        </Card.Text>
                        <Card.Title>Ingredients:</Card.Title>
                        <Card.Text>
                            <div style= {{display: 'flex', justifyContent: 'center', padding: "0em 3em",}}>
                            {recipe.recipe_ingredients}
                            </div>
                        </Card.Text>
                        <Button href={recipe.recipe_instructions} variant="success">Get full recipe</Button>
                    </Card.Body>
                <div style={{justifyContent: 'center', padding: "1em 3em",}}>
                    <CardGroup>
                        <Card>
                            <Card.Header >Farmers Market with Ingredients</Card.Header>
                            <Card.Body>
                            <Card.Text>
                                {allLocations.map(location => (
                                    location.location_products.toLowerCase().includes(category.at(0)) ? 
                                        <Card.Link href={`/locations/${location.id}`}> {location.location_name} <br/> </Card.Link>
                                        : <></>
                                ))}
                            </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card>
                            <Card.Header >Ingredients sold in Markets</Card.Header>
                            <Card.Body>
                            <Card.Text>
                                {allProducts.map(product => (
                                    products.includes(product.produce_name.toLowerCase()) ? 
                                        <Card.Link href={`/products/${product.id}`}> {product.produce_name} <br/> </Card.Link>
                                        : <></>
                                ))}
                            </Card.Text>
                            </Card.Body>
                        </Card>
                    </CardGroup>
                </div>
            <Card.Footer className="text-muted"><Card.Link href="/recipes?page=1">View more recipes</Card.Link></Card.Footer>
            </Card>
        </div>
        );
    }

    return (
        <>
            {loading ? <Loading /> : <ShowRecipe />}
        </>);
}

export default Recipe;
