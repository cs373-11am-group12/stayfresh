import React, { useEffect, useState } from 'react';
import axios from 'axios';
import RingLoader from "react-spinners/RingLoader";
import {
    CardImg,
    Card,
    Row,
    Col
} from 'react-bootstrap';
import {
    StringParam,
    NumberParam,
    useQueryParams,
    withDefault,
} from 'use-query-params';
import Pagination from '../../Pagination';
import SearchBar from '../../searchbar/SearchBar';
import Highlighter from "react-highlight-words";

const Recipes = () => {
    const endpoint = 'https://api.stay-fresh.me/recipes';
    const [recipes, setRecipes] = useState([]);
    const [loading, setLoading] = useState(false);
    const [numRecipes] = useState(10);

    const [params, setParams] = useQueryParams({
        page: withDefault(NumberParam, 1),
        q: withDefault(StringParam, ""),
        sort: withDefault(StringParam, ""),
        meal: withDefault(StringParam, ""),
        type: withDefault(StringParam, ""),
        health: withDefault(StringParam, ""),
        cautions: withDefault(StringParam, ""),
        ingredients: withDefault(StringParam, "")
    });

    useEffect(() => {
        setLoading(true);
        let URLParams = new URLSearchParams();
        URLParams.append("q", params.q);
        if (params.sort)
            URLParams.append("sort", params.sort)
        if (params.meal)
            URLParams.append("meal", params.meal)
        if (params.type)
            URLParams.append("rtype", params.type)
        if (params.health)
            URLParams.append("health", params.health)
        if (params.cautions)
            URLParams.append("cautions", params.cautions)
        if (params.ingredients)
            URLParams.append("ingredients", params.ingredients)
        axios
            .get(`${endpoint}?${URLParams}`)
            .then((response) => {
                console.log(response);
                setRecipes(response.data['page']);
                setLoading(false);
            })
            .catch(e => console.log(e));
      }, [params]);

    const lastIndex = params.page * numRecipes;
    const firstIndex = lastIndex - numRecipes;
    const currentRecipes = recipes.slice(firstIndex, lastIndex);

    const paginate = page => setParams({ ...params, page: page });

    const override = 
    `
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%)
    `;
    const Loading = () => 
        <RingLoader 
            color={'#7ED381'}
            loading={loading}
            css={override}
            size={150}
        />;

    const ShowRecipes = () => {
        return (
            <>
                <Row xs={1} md={5} className="g-4">
                    {currentRecipes.map(recipe => (
                        <Col key={recipe.id}>
                            <Card width = "20%" src={recipe}>
                            <Card.Link href={`/recipes/${recipe.id}`}>
                                <CardImg width = "20%" src={recipe.recipe_picture} alt="img" />
                                <Card.Body>
                                    <h1><Highlighter autoEscape={true} textToHighlight={recipe.recipe_name} searchWords={params.q.split(/[%20 \s,;/]+/)}/></h1>
                                    <Card.Text>
                                        <b>Meal Type:</b><br/> <Highlighter autoEscape={true} textToHighlight={recipe.recipe_meal} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                    <Card.Text>
                                        <b>Cuisine Type:</b><br/> <Highlighter autoEscape={true} textToHighlight={recipe.recipe_type} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                    <Card.Text>
                                        <b>Health labels:</b><br/> <Highlighter autoEscape={true} textToHighlight={recipe.recipe_health} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                    <Card.Text>
                                        <b>Cautions:</b><br/> 
                                        <Highlighter autoEscape={true} textToHighlight={recipe.recipe_cautions} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                    <Card.Text>
                                        <b>Nutrients:</b><br/>
                                        Total Calories: {Number.parseFloat(recipe.recipe_calories).toFixed(2)}<br/>
                                        Total Fat: {Number.parseFloat(recipe.recipe_fat).toFixed(2)}<br/>
                                        Protein: {Number.parseFloat(recipe.recipe_protein).toFixed(2)}<br/>
                                        Sugar: {Number.parseFloat(recipe.recipe_sugar).toFixed(2)}<br/>
                                    </Card.Text>
                                </Card.Body>
                                </Card.Link>
                            </Card>
                        </Col>
                    ))}
                </Row>
                <Pagination 
                    instancesPer={numRecipes}
                    totalInstances={recipes.length}
                    paginate={paginate}
                    page={params.page}
                />
            </>
        );
    }

    return (
        <>
            <SearchBar
                    filters={["Meal", "Type", "Health", "Cautions", "Ingredients"]}
                    filterOptions={[
                        ["breakfast", "lunch/dinner", "brunch", "snack", "teatime"],
                        ["american", "french", "british", "indian", "middle eastern", "asian", "mexican", "chinese", "nordic", "caribbean", "japanese", "south east asian", "central europe", "mediterranean", "south american", "italian"],
                        ["Sugar-Conscious","Pescatarian","Mediterranean","Dairy-Free","Peanut-Free","Tree-Nut-Free","Soy-Free","Fish-Free","Pork-Free","Red-Meat-Free","Celery-Free","Mustard-Free","Sesame-Free","Lupine-Free", "Vegetarian","Gluten-Free", "Egg-Free", "Alcohol-Free"],
                        ["Sulfites", "FODMAP", "Gluten", "Wheat", "Tree-Nuts", "Egg", "Milk", "Soy"],
                        ["pea", "potato", "chicken", "eggplant", "sour cream", "vanilla", "sausage", "broccoli", "flour", "paneer", "lemon", "orange", "milk", "bell pepper", "fish", "onion", "tomato", "mango", "honey", "apple", "plum", "blackberry", "butter", "lentil", "shrimp", "soy sauce", "bok choy", "ketchup",
                                "coriander", "chili pepper", "garlic", "ginger", "pepper", "salt", "olive", "olive oil", "peanut butter", "pork", "spinach", "mushroom", "basil", "kale", "cinnamon", "duck", "sesame oil", "kidney bean", "carrots", "celery", "bread", "jalapeno", "mayonnaise", "lettuce", "banana", "strawberry",
                                "oatmeal", "wine", "yogurt", "red onion", "yellow onion", "mint", "chocolate", "raspberry", "tofu", "shallot", "walnut", "salmon", "avocado", "thyme", "cabbage", "bacon", "cumin", "mustard", "mozzarella cheese", "ham", "parsley", "vinegar", "anchovy", "paprika", "rice", "dill"]
                    ]}
                    sorts ={["Name (ascending)", "Name (descending)", "Meal (ascending)", "Meal (descending)", "Type (ascending)", "Health (descending)", "Cautions (ascending)", "Cautions descending"]}
                />
            {loading ? <Loading /> : <ShowRecipes />}
        </>
    );
}

export default Recipes;
