import React, { useEffect, useState } from 'react';
import axios from 'axios';
import RingLoader from "react-spinners/RingLoader";
import {
    CardImg,
    Card,
    Row,
    Col
} from 'react-bootstrap';
import {
    StringParam,
    useQueryParams,
    withDefault,
} from 'use-query-params';
import SearchBar from '../searchbar/SearchBar';
import Highlighter from "react-highlight-words";

const Search = () => {
    const productsEndpoint = 'https://api.stay-fresh.me/products';
    const recipesEndpoint = 'https://api.stay-fresh.me/recipes';
    const locationsEndpoint = 'https://api.stay-fresh.me/locations';

    const [products, setProducts] = useState([]);
    const [recipes, setRecipes] = useState([]);
    const [locations, setLocations] = useState([]);

    const [loading, setLoading] = useState(false);

    const [params, setParams] = useQueryParams({
        q: withDefault(StringParam, "")
    });

    useEffect(() => {
        setLoading(true);
        let URLParams = new URLSearchParams();
        URLParams.append("q", params.q);
        if (params.sort)
            URLParams.append("sort", params.sort)

        console.log(`${productsEndpoint}?${URLParams}`);
        
        const getProducts = axios.get(`${productsEndpoint}?${URLParams}`)
        const getRecipes = axios.get(`${recipesEndpoint}?${URLParams}`);
        const getLocations = axios.get(`${locationsEndpoint}?${URLParams}`);
        console.log(params.q.split(/[%20 \s,;/]+/));
        
        axios
            .all([getProducts, getRecipes, getLocations])
            .then(responses => {
                console.log(responses);
                setProducts(responses[0].data['page']);
                setRecipes(responses[1].data['page']);
                setLocations(responses[2].data['page']);
                setLoading(false);
            })
            .catch(e => console.log(e))
      }, [params]);

    const override = 
    `
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%)
    `;
    const Loading = () => 
        <RingLoader 
            color={'#7ED381'}
            loading={loading}
            css={override}
            size={150}
        />;

      const currentRecipes = recipes.slice(0, 9);
      const currentLocations = locations.slice(0, 9);
      const currentProducts = products.slice(0, 9);

      const ShowProducts = () => {
        return (
            <>
                <Row xs={1} md={5} className="g-4">
                    {currentProducts.map(product => (
                        <Col key={product.id}>
                            <Card width = "20%" src={product}>
                                <Card.Link href={`/products/${product.id}`}>
                                    <Card.Body>
                                        <h1><Highlighter autoEscape={true} textToHighlight={product.produce_name} searchWords={params.q.split(/[%20 \s,;/]+/)}/></h1>
                                        <CardImg width = "20%" src={product.produce_picture} alt="product image" />
                                        <Card.Title>Nurtition Facts</Card.Title>
                                        <Card.Text>
                                            Calories: {Number.parseFloat(product.produce_energy).toFixed(2)} kcal <br/>
                                            Protein: {Number.parseFloat(product.produce_protein).toFixed(2)} g<br/>
                                            Fat: {Number.parseFloat(product.produce_fat).toFixed(2)} g<br/>
                                            Carbs: {Number.parseFloat(product.produce_carbs).toFixed(2)} g<br/>
                                            Weight: {Number.parseFloat(product.produce_weight).toFixed(2)} g<br/>
                                            <br/>
                                        </Card.Text>
                                    </Card.Body>
                                </Card.Link>
                            </Card>
                        </Col>
                    ))}
                    <Card width = "20%">
                        <Card.Link href={`/products?page=1&q=${params.q}`}>
                            <Card.Body>
                                <h1>Click here to view more products</h1>
                            </Card.Body>
                        </Card.Link>
                    </Card>
                </Row>
            </>
        );
    }

    const ShowRecipes = () => {
        return (
            <>
                <Row xs={1} md={5} className="g-4">
                    {currentRecipes.map(recipe => (
                        <Col key={recipe.id}>
                            <Card width = "20%" src={recipe}>
                            <Card.Link href={`/recipes/${recipe.id}`}>
                                <CardImg width = "20%" src={recipe.recipe_picture} alt="img" />
                                <Card.Body>
                                <h1><Highlighter autoEscape={true} textToHighlight={recipe.recipe_name} searchWords={params.q.split(/[%20 \s,;/]+/)}/></h1>
                                    <Card.Text>
                                        <b>Meal Type:</b><br/> <Highlighter autoEscape={true} textToHighlight={recipe.recipe_meal} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                    <Card.Text>
                                        <b>Cuisine Type:</b><br/> <Highlighter autoEscape={true} textToHighlight={recipe.recipe_type} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                    <Card.Text>
                                        <b>Health labels:</b><br/> <Highlighter autoEscape={true} textToHighlight={recipe.recipe_health} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                    <Card.Text>
                                        <b>Cautions:</b><br/> 
                                        <Highlighter autoEscape={true} textToHighlight={recipe.recipe_cautions} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                    <Card.Text>
                                        <b>Nutrients:</b><br/>
                                        Total Calories: {Number.parseFloat(recipe.recipe_calories).toFixed(2)}<br/>
                                        Total Fat: {Number.parseFloat(recipe.recipe_fat).toFixed(2)}<br/>
                                        Protein: {Number.parseFloat(recipe.recipe_protein).toFixed(2)}<br/>
                                        Sugar: {Number.parseFloat(recipe.recipe_sugar).toFixed(2)}<br/>
                                    </Card.Text>
                                </Card.Body>
                                </Card.Link>
                            </Card>
                        </Col>
                    ))}
                    <Card width = "20%">
                        <Card.Link href={`/recipes?page=1&q=${params.q}`}>
                            <Card.Body>
                                <h1>Click here to view more recipes</h1>
                            </Card.Body>
                        </Card.Link>
                    </Card>
                </Row>
            </>
        );
    }

    const ShowLocations = () => {
        return (
            <>
                <Row xs={1} md={5} className="g-4">
                    {currentLocations.map(location => (
                        <Col key={location.id}>
                            <Card width = "20%" src={location}>
                                <Card.Link href={`/locations/${location.id}`}> 
                                <CardImg width = "20%" src={location.picture} alt="Market Picture" />
                                <Card.Body>
                                <Card.Title><Highlighter autoEscape={true} textToHighlight={location.location_name} searchWords={params.q.split(/[%20 \s,;/]+/)}/></Card.Title>
                                <Card.Title>Products</Card.Title>
                                <Card.Text>
                                    <Highlighter autoEscape={true} textToHighlight={location.location_products} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    <br/>
                                    <Card.Title>Schedule:</Card.Title>
                                    <Card.Text>
                                    <Highlighter autoEscape={true} textToHighlight={location.location_schedule.replace(";<br> <br> <br>","")} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                    <Card.Title>Address</Card.Title>
                                    <Card.Text>
                                    <Highlighter autoEscape={true} textToHighlight={location.location_address} searchWords={params.q.split(/[%20 \s,;/]+/)}/>
                                    </Card.Text>
                                </Card.Text>
                                </Card.Body>
                                </Card.Link>
                            </Card>
                        </Col>
                    ))}
                    <Card width = "20%">
                        <Card.Link href={`/locations?page=1&q=${params.q}`}>
                            <Card.Body>
                                <h1>Click here to view more locations</h1>
                            </Card.Body>
                        </Card.Link>
                    </Card>
                </Row>
            </>
        );
    };

    return (
        <>
            <SearchBar
                filters={[]}
                filterOptions={[]}
                sorts={[]}
            />
            {loading ? <Loading /> : 
            <>
                <ShowProducts />
                <ShowRecipes />
                <ShowLocations />
            </>
            }
        </>
    );
}

export default Search;
