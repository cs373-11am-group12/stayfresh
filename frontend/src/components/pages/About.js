import React from 'react';
import { useState, useEffect } from 'react';
import axios from 'axios';
import RingLoader from "react-spinners/RingLoader";
import CardGroup from 'react-bootstrap/CardGroup';
import Card from 'react-bootstrap/Card';
import maria from './imgs/maria.jpg'
import francisco from './imgs/francisco.jpg'
import william from './imgs/william.jpg'
import ziyi from './imgs/ziyi.jpg'
import sanchith from './imgs/sanchith.png'
import gitlab from './imgs/gitlab.png'
import awsamplify from './imgs/awsamplify.png'
import bootsrap from './imgs/bootstrap.png'
import namecheap from './imgs/namecheap.png'
import nodejs from './imgs/nodejs.png'
import postman from './imgs/postman.png'
import react from './imgs/react.png'

const About = () => {
    const [data, setData] = useState([]);
    const [numIssues, setNumIssues] = useState(null);
    const [loading, setLoading] = useState(false);
    const fetchData = () => {
        const pages = [];
        var page = 1;
        while (page < 10) {
            const commitsPerPageAPI = "https://gitlab.com/api/v4/projects/33936179/repository/commits?per_page=100&page="+page;
            const getCommitsPerPage = axios.get(commitsPerPageAPI);
            pages.push(getCommitsPerPage)
            page++;
        }
        axios.all(pages).then(
            axios.spread((...allPages) => {
                let tmpData = [];
                for (let i = 0; i < allPages.length; ++i) {
                    for (let j = 0; j < allPages[i].data.length; j++) {
                        tmpData.push(allPages[i].data[j])
                    }
                }
                setData(tmpData);
            })

        )
    }
    const fetchNumIssues = () => {
        const numIssuesAPI = "https://gitlab.com/api/v4/projects/33936179/issues?per_page=100";
        const getNumIssues = axios.get(numIssuesAPI);
        axios.all([getNumIssues]).then(axios.spread((...numIssues) => {
            setNumIssues(numIssues[0].data)
        }))
    }
    useEffect(()=> {
        setLoading(true);
        fetchData();
        fetchNumIssues();
        setLoading(false);
    }, [])

    const override = 
    `
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%)
    `;
    const Loading = () => 
        <RingLoader 
            color={'#7ED381'}
            loading={loading}
            css={override}
            size={150}
        />;
    var repos = 
        <CardGroup>
            <Card>
                <Card.Link href='https://gitlab.com/cs373-11am-group12/stayfresh'>
                <Card.Header style={{fontFamily: "Tahoma", justifyContent: 'center',}}>Gitlab REPO</Card.Header>
                </Card.Link>
            </Card>
            <Card>
                <Card.Link href='https://martian-desert-25411.postman.co/workspace/StayFresh~350ed79e-ae36-4279-a664-cc266c0ec4cd/overview'>
                <Card.Header style={{fontFamily: "Tahoma", justifyContent: 'center',}}>Postman Doc</Card.Header>
                </Card.Link>
            </Card>
        </CardGroup>
    var counts = 
    <div style={{fontFamily: "Tahoma", justifyContent: 'center',}}>
        <CardGroup>
            <Card>
                <Card.Body>
                    <Card.Title>Issues</Card.Title>
                    <Card.Text>{numIssues != null && numIssues.length}</Card.Text>
                </Card.Body>
            </Card>
            <Card>
                <Card.Body>
                    <Card.Title>Unit Tests</Card.Title>
                    <Card.Text>10</Card.Text>
                </Card.Body>
            </Card>
            <Card>
                <Card.Body>
                    <Card.Title>Commits(Development branch)</Card.Title>
                    <Card.Text>{data.length > 0 && data.length}</Card.Text>
                </Card.Body>
            </Card>
        </CardGroup>
    </div>
    var about = 
    <div>
        <h1 style={{fontFamily: "Tahoma", textAlign: "center"}}>What is StayFresh?</h1>
        <p style={{fontFamily: "Tahoma", textAlign: "left"}}>
        StayFresh is meant for the everyday grocery consumer and home chef who is looking for a healthier
        lifestyle change and a way to support the local community. Shopping at local farmer's markets in a
        "farm-to-table" approach ensures that your food is sourced locally, is in season, and is as fresh as possible
        (without the worry of preservatives). This can go a long way to make each bite of your food is tastier than ever.
        StayFresh provides nutritional information about meats and produce and connects you with local farmer's markets where
        you can obtain those ingredients, allowing you to support local producers in your area. Additionally, check out some
        tasty recipes you can make with the ingredients you just bought, just with a click of a button on StayFresh!
        </p>
    </div>
    var developers = 
    <div style={{fontFamily: "Tahoma", display: 'flex', justifyContent: 'center', padding: "3em 2em",}}>
        <CardGroup>
            <Card>
            <Card.Header>Frontend Developer | Phase I Leader</Card.Header>
            <Card.Img variant="top" src={maria} />
            <Card.Body>
                <Card.Title>Maria Gu</Card.Title>
                <Card.Subtitle>gitlab ID: mgu1115</Card.Subtitle>
                <Card.Text >
                Hello, I am a third year CS major who is interested in Web/App development, 
                specifically front-end but would love to learn some backend as well. 
                Outside of programming, my hobbies include dance and rock climbing.
                </Card.Text>
            </Card.Body>
            <Card.Footer>
                <small>number of commits: {data.length > 0 && data.filter(x => x['committer_name'] === 'mgu1115' || x['committer_name'] === 'Maria Gu').length}<br/></small>
                <small>number of issues contributed: {numIssues != null && numIssues.filter(x => x['assignee'] != null && x['assignee']['name'] === 'Maria Gu').length}<br/></small>
                <small>number of unit tests contributed: 10</small>
            </Card.Footer>
            
            </Card>
        <Card>
        <Card.Header>Frontend Developer</Card.Header>
        <Card.Img variant="top" src={ziyi}/>
        <Card.Body>
            <Card.Title>Ziyi Zhao</Card.Title>
            <Card.Subtitle>gitlab ID: zhaozyi</Card.Subtitle>
            <Card.Text>
            I'm a CS major student at University of Texas at Austin, and I'm interesting in designing softwares as well as developing games.
            Currently my responsibility is mostly front-end stuff, scraping data from gitlab. My hobbies include swimming and playing video games.
            </Card.Text>
        </Card.Body>
        <Card.Footer>
            <small>number of commits: {data.length > 0 && data.filter(x => x['committer_name'] === 'John Zhao' || x['committer_name'] === 'Ziyi Zhao' || x['committer_name'] === 'zhaozyi').length}<br/></small>
            <small>number of issues contributed: {numIssues != null && numIssues.filter(x => x['assignee'] != null && x['assignee']['name'] === 'Ziyi Zhao').length}<br/></small>
            <small>number of unit tests contributed: 0</small>
        </Card.Footer>
        </Card>
        <Card>
        <Card.Header>Full Stack Developer | Phase II Leader</Card.Header>
        <Card.Img variant="top" src={sanchith} />
        <Card.Body>
            <Card.Title>Sanchith Shanmuga</Card.Title>
            <Card.Subtitle>gitlab ID: sanchiths</Card.Subtitle>
            <Card.Text>

            Hi! I'm a third year CS major at UT Austin, and I'm interested in learning about big data and machine learning.
            I've worked on the front-end of this project so far (along with other non-programming tasks), but I look forward to
            working on the back-end in future phases. Outside of school, I enjoy dancing with a competitive team, playing basketball, and
            playing video games.
            </Card.Text>
        </Card.Body>
        <Card.Footer>
            <small>number of commits: {data.length > 0 && data.filter(x => x['committer_name'] === 'sanchiths' || x['committer_name'] === 'Sanchith Shanmuga').length}<br/></small>
            <small>number of issues contributed: {numIssues != null && numIssues.filter(x => x['assignee'] != null && x['assignee']['name'] === 'Sanchith Shanmuga').length}<br/></small>
            <small>number of unit tests contributed: 0</small>
        </Card.Footer>
        </Card>
        <Card>
        <Card.Header>Backend Developer | Phase III Leader</Card.Header>
        <Card.Img variant="top" src={william} />
        <Card.Body>
            <Card.Title>William Gunawan</Card.Title>
            <Card.Subtitle>gitlab ID: wgunawan2000</Card.Subtitle>
            <Card.Text>
            I'm a third year CS major at UT Austin. I grew up in Austin, Texas and spend
            a lot of my time swimming, working out, playing games, and watching anime.
            </Card.Text>
        </Card.Body>
        <Card.Footer>
            <small>number of commits: {data.length > 0 && data.filter(x => x['committer_name'] === 'wgunawan2000' || x['committer_name'] === 'William Gunawan').length}<br/></small>
            <small>number of issues contributed: {numIssues != null && numIssues.filter(x => x['assignee'] != null && x['assignee']['name'] === 'wgunawan2000').length}<br/></small>
            <small>number of unit tests contributed: 0</small>
        </Card.Footer>
        </Card>
        <Card>
        <Card.Header>Frontend Developer | Phase IV Leader</Card.Header>
        <Card.Img variant="top" src={francisco} />
        <Card.Body>
            <Card.Title>Francisco Villegas</Card.Title>
            <Card.Subtitle>gitlab ID: franky52</Card.Subtitle>
            <Card.Text>
            I'm a third year CS major at UT Austin hoping to get a concentration in Computer Systems 
            or Cybersecurity. My hobbies include playing basketball, running, working out, and playing 
            video games.
            </Card.Text>
        </Card.Body>
        <Card.Footer>
            <small>number of commits: {data.length > 0 && data.filter(x => x['committer_name'] === 'franky52' || x['committer_name'] === 'Francisco Villegas').length}<br/></small>
            <small>number of issues contributed: {numIssues != null && numIssues.filter(x => x['assignee'] != null && x['assignee']['name'] === 'Francisco Villegas').length}<br/></small>
            <small>number of unit tests contributed: 0</small>
        </Card.Footer>
        </Card>
    </CardGroup>
    </div>
    var links = <div style={{fontFamily: "Tahoma", justifyContent: 'center', padding: "2em 0em",}}>
        <h2 style={{fontFamily: "Tahoma", textAlign: "center"}}>APIs</h2>
        <CardGroup>
            <Card>
                <Card.Title><a href='https://search.ams.usda.gov/farmersmarkets/v1/svcdesc.html'>Farmers Market Location API</a></Card.Title>
                <Card.Subtitle>fetch(url)(JS Function) Data From URL:</Card.Subtitle>
                <Card.Body>
                    <li><a href='http://search.ams.usda.gov/farmersmarkets/v1/data.svc/mktDetail?id=1016803'>Market 1 Data</a></li>
                    <li><a href='http://search.ams.usda.gov/farmersmarkets/v1/data.svc/mktDetail?id=1021616'>Market 2 Data</a></li>
                    <li><a href='http://search.ams.usda.gov/farmersmarkets/v1/data.svc/mktDetail?id=1021534'>Market 3 Data</a></li>
                </Card.Body>
            </Card>
            <Card>
                <Card.Title><a href="https://api.edamam.com/">Production Information API</a></Card.Title>
                <Card.Subtitle>fetch(url)(JS Function) Data From URL:</Card.Subtitle>

                <Card.Body>
                    <li><a href='https://api.edamam.com/api/food-database/v2/parser?app_id=63118ec8&app_key=bc6775a1de0121148827c389eaf968cf&ingr=apple'>Product 1 Data</a></li>
                    <li><a href='https://api.edamam.com/api/food-database/v2/parser?app_id=63118ec8&app_key=bc6775a1de0121148827c389eaf968cf&ingr=egg'>Product 2 Data</a></li>
                    <li><a href='https://api.edamam.com/api/food-database/v2/parser?app_id=63118ec8&app_key=bc6775a1de0121148827c389eaf968cf&ingr=tomato'>Product 3 Data</a></li>
                </Card.Body>
            </Card>
            <Card>
                <Card.Title><a href="https://api.edamam.com/">Recipes API</a></Card.Title>
                <Card.Subtitle>fetch(url)(JS Function) Data From URL:</Card.Subtitle>

                <Card.Body>
                    <li><a href='https://api.edamam.com/api/recipes/v2?type=public&q=Breakfast%20Taco&app_id=183a0ef1&app_key=41663171c2abb8782209018eb66d7f9d'>Recipe 1 Data</a></li>
                    <li><a href='https://api.edamam.com/api/recipes/v2?type=public&q=Apple%20Pie&app_id=183a0ef1&app_key=41663171c2abb8782209018eb66d7f9d'>Recipe 2 Data</a></li>
                    <li><a href='https://api.edamam.com/api/recipes/v2?type=public&q=Sandwhich%20&app_id=183a0ef1&app_key=41663171c2abb8782209018eb66d7f9d'>Recipe 3 Data</a></li>
                </Card.Body>
            </Card>
            <Card>
                <Card.Title><a href="https://docs.gitlab.com/ee/api/api_resources.html">Gitlab API</a></Card.Title>
                <Card.Subtitle>fetch(url)(JS Function) Data From URL:</Card.Subtitle>

                <Card.Body>
                    <li><a href="https://gitlab.com/api/v4/projects/33936179/repository/commits?ref_name=Development&per_page=100">Commits Data</a></li>
                    <li><a href='https://gitlab.com/api/v4/projects/33936179/issues'>Issues Data</a></li>
                </Card.Body>
            </Card>
            <Card>
                <Card.Title><a href="https://documenter.getpostman.com/view/19741112/UVksLuG8">StayFresh API</a></Card.Title>
                <Card.Subtitle>Designed using:</Card.Subtitle>
                <Card.Body>
                    <li><a href="https://www.postman.com/">Postman</a></li>
                </Card.Body>
            </Card>
        </CardGroup>

    </div>
    var tools =
    <div style={{fontFamily:"Tahoma"}}>
        <h2 style={{fontFamily: "Tahoma", textAlign: "center"}}>Tools Used</h2>
        <CardGroup>
            <Card onClick={() => {window.open('https://aws.amazon.com/amplify/')}} style={{cursor: 'pointer'}}>
                <Card.Img src={awsamplify} />
                <Card.Title>AWS Amplify</Card.Title>
                <Card.Subtitle>How We Used</Card.Subtitle>
                <Card.Body>
                    <Card.Text>
                            <li>Used for holding domain of our website.</li>
                            <li>Sign up</li>
                            <li>import code repo from Gitlab</li>
                            <li>Put the domain of our website into the domain</li>
                    </Card.Text>
                </Card.Body>
            </Card>
            <Card onClick={() => {window.open("https://www.postman.com/")}} style={{cursor: 'pointer'}}>
                <Card.Img src={postman} />
                <Card.Title>Postman</Card.Title>
                <Card.Subtitle>How We Used</Card.Subtitle>
                <Card.Body>
                    <Card.Text>
                        <li>Used for deigning RESTful API for our website</li>
                        <li>Sign up</li>
                        <li>Document all the data request methods and the data returned</li>
                    </Card.Text>
                </Card.Body>
            </Card>
            <Card onClick={() => {window.open("https://reactjs.org/")}} style={{cursor: 'pointer'}}>
                <Card.Img src={react} />
                <Card.Title>React</Card.Title>
                <Card.Subtitle>How We Used</Card.Subtitle>
                <Card.Body>
                    <Card.Text>
                        <li>Javascript library that provides helpful components to use during the development process</li>
                        <li>Import Components from React if needed at the beginning of a JS file</li>
                    </Card.Text>
                </Card.Body>
            </Card>
            <Card onClick={() => {window.open("https://nodejs.org/en/")}} style={{cursor: 'pointer'}}>
                <Card.Img src={nodejs} />
                <Card.Title>Node.js</Card.Title>
                <Card.Subtitle>How We Used</Card.Subtitle>
                <Card.Body>
                    <Card.Text>
                        <li>Javascript runtime built</li>
                        <li>Run npx create-react-app my-app command to create directories needed for the web</li>
                        <li>Run npm start to run the web app in development mode and view it at http://localhost:3000 in the browser</li>
                    </Card.Text>
                </Card.Body>
            </Card>
            <Card onClick={() => {window.open("https://getbootstrap.com/")}} style={{cursor: 'pointer'}}>
                <Card.Img src={bootsrap} />
                <Card.Title>Bootstrap</Card.Title>
                <Card.Subtitle>How We Used</Card.Subtitle>
                <Card.Body>
                    <Card.Text>
                        <li>Front-end open source toolkit</li>
                        <li>Included in React.</li>
                        <li>Simply type the style name and assign it to a CSS attribute if React is included</li>
                    </Card.Text>
                </Card.Body>
            </Card>
            <Card onClick={() => {window.open("https://www.namecheap.com/")}} style={{cursor: 'pointer'}}>
                <Card.Img src={namecheap} />
                <Card.Title>Namecheap</Card.Title>
                <Card.Subtitle>How We Used</Card.Subtitle>
                <Card.Body>
                    <li>Hostname provider</li>
                    <li>Sign up and get a free hostname</li>
                    <li>Follow the instructions on the web and set up the records for the website we build</li>
                </Card.Body>
            </Card>
            <Card onClick={() => {window.open("https://about.gitlab.com/")}} style={{cursor: 'pointer'}}>
                <Card.Img src={gitlab} />
                <Card.Title>Gitlab</Card.Title>
                <Card.Subtitle>How We Used</Card.Subtitle>
                <Card.Body>
                    <li>Code Repository</li>
                    <li>Use git commands to control the version of our code</li>
                </Card.Body>
            </Card>
        </CardGroup>
    </div>

    var res = <div style={({ margin: '2.5rem' })}><React.Fragment>
        {about}
        {repos}
        {counts}
        {developers}
        {links}
        {tools}
    </React.Fragment></div>
    return (loading ? <Loading /> : data.length > 0 && res);
}

export default About;
