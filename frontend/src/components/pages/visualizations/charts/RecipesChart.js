import React, { useEffect, useState } from 'react';
import { PieChart, Pie } from 'recharts';
import axios from 'axios';

const RecipesChart = () => {
    const endpoint = 'https://api.stay-fresh.me/recipes';
    const [recipes, setRecipes] = useState([]);

    useEffect(() => {
        axios
        .get(`${endpoint}`)
        .then((response) => setRecipes(response.data['page'].map(recipe => recipe.recipe_meal)))
        .catch(e => console.log(e));
    }, []);

    let meals = new Map();
    recipes.forEach(meal => meals.set(meal, meals.get(meal) === undefined ? 0 : meals.get(meal) + 1));
    let newRecipes = [];
    let mealKeys = Array.from(meals.keys());
    for (let i = 0; i < mealKeys.length; ++i)
        newRecipes.push({ meal: mealKeys[i], count: meals.get(mealKeys[i]) });

    let renderLabel = entry =>  entry.meal;
    
    return (
        <>
            <h2>Types of Recipes</h2>
            <PieChart width={750} height={250}>
                <Pie data={newRecipes} dataKey="count" label={renderLabel} nameKey="count" fill="green"/>
            </PieChart>
        </>
    )
}

export default RecipesChart;
