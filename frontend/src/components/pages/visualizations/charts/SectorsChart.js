import React, { useEffect, useState } from 'react';
import { PieChart, Pie, Tooltip } from 'recharts';
import axios from 'axios';

const SectorsChart = () => {
    const endpoint = 'https://api.getthatbread.me/api/companies';
    const [companies, setCompanies] = useState([]);

    useEffect(() => {
        axios
        .get(`${endpoint}`)
        .then((response) => setCompanies(response.data['data'].map(company => company.sector)))
        .catch(e => console.log(e));
    }, []);

    let sectors = new Map();
    companies.forEach(sector => sectors.set(sector, sectors.get(sector) === undefined ? 0 : sectors.get(sector) + 1));
    let newCompanies = [];
    let sectorKeys = Array.from(sectors.keys());
    for (let i = 0; i < sectorKeys.length; ++i)
        newCompanies.push({ sector: sectorKeys[i], count: sectors.get(sectorKeys[i]) });
    
    let renderLabel = entry =>  entry.count > 1 ? entry.sector : "";

    return (
        <>
            <h2>Company Sectors</h2>
            <PieChart width={850} height={650}>
                <Pie label={renderLabel} data={newCompanies} dataKey="count" nameKey="count" fill="blue"/>
            </PieChart>
        </>
    )
}

export default SectorsChart;
