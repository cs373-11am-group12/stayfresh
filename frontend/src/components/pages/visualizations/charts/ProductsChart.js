import React, { useEffect, useState } from 'react';
import { ScatterChart, Scatter, CartesianGrid, XAxis, YAxis, ZAxis, Tooltip } from 'recharts';
import axios from 'axios';

const ProductsChart = () => {
    const endpoint = 'https://api.stay-fresh.me/products?sort=carbs';
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        axios
        .get(`${endpoint}`)
        .then((response) => setProducts(response.data['page']))
        .catch(e => console.log(e));
    }, []);

    return (
        <>
            <h2>Carbs vs. Energy</h2>
            <ScatterChart width={750} height={250}
            margin={{ top: 20, right: 20, bottom: 10, left: 10 }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="produce_carbs" name="Carbs" unit=" g" />
                <YAxis dataKey="produce_energy" name="Energy" unit=" kCal" />
                <ZAxis dataKey="produce_name" name="Name" unit="" />
                <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                <Scatter data={products} fill="green" />
            </ScatterChart>
            
        </>
    )
}

export default ProductsChart;
