import React, { useEffect, useState } from 'react';
import { ScatterChart, Scatter, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';
import axios from 'axios';

const CitiesChart = () => {
    const endpoint = 'https://api.getthatbread.me/api/cities?sort=rating';
    const [cities, setCities] = useState([]);

    useEffect(() => {
        axios
        .get(`${endpoint}`)
        .then((response) => setCities(response.data['data']))
        .catch(e => console.log(e));
    }, []);

    return (
        <>
            <h2>City Ratings</h2>
            <ScatterChart width={750} height={250}
                margin={{ top: 20, right: 20, bottom: 10, left: 10 }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="cityName" name="Name" />
                <YAxis dataKey="average_rating" name="Rating" />
                <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                <Scatter data={cities} fill="blue" />
            </ScatterChart>
        </>
    )
}

export default CitiesChart;
