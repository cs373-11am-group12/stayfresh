import React, { useEffect, useState } from 'react';
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';
import axios from 'axios';

const CompaniesChart = () => {
    const endpoint = 'https://api.getthatbread.me/api/companies?sort=employees';
    const [companies, setCompanies] = useState();

    useEffect(() => {
        axios
        .get(`${endpoint}`)
        .then((response) => setCompanies(response.data['data']))
        .catch(e => console.log(e));
    }, [])

    return (
        <>
            <h2>Companies and Employees</h2>
            <BarChart width={750} height={250} data={companies}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="companyName" />
                <YAxis />
                <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                <Legend />
                    <Bar dataKey="employee_lower_bound" fill="blue" />
            </BarChart>
            
        </>
    )
}

export default CompaniesChart;
