import React, { useEffect, useState } from 'react';
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';
import axios from 'axios';

const LocationsChart = () => {
    const endpoint = 'https://api.stay-fresh.me/locations';
    const [locations, setLocations] = useState([]);

    useEffect(() => {
        axios
        .get(`${endpoint}`)
        .then((response) => {
            setLocations(response.data['page'].map(location => {
                let address = location.location_address.split(', ');
                return address[address.length - 2];
            }));
        })
        .catch(e => console.log(e));
    }, []);

    let states = new Map();
    locations.forEach(state => states.set(state, states.get(state) === undefined ? 1 : states.get(state) + 1));
    let newLocations = [];
    let stateKeys = Array.from(states.keys());
    for (let i = 0; i < stateKeys.length; ++i)
        if (states.get(stateKeys[i]) > 1)
            newLocations.push({ state: stateKeys[i], count: states.get(stateKeys[i]) })

    return (
        <>
            <h2>Markets at Locations</h2>
            <BarChart width={750} height={250} data={newLocations}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="state" />
                <YAxis dataKey="count" />
                <Tooltip />
                <Legend />
                    <Bar dataKey="count" fill="green" />
            </BarChart>
        </>
    )
}

export default LocationsChart;
