// connect all visualization components (graphs) on this one page, then connect this with App.js
import React from 'react';
import ProductsChart from './charts/ProductsChart';
import RecipesChart from './charts/RecipesChart';
import LocationsChart from './charts/LocationsChart';
import CompaniesChart from './charts/CompaniesChart';
import SectorsChart from './charts/SectorsChart';
import CitiesChart from './charts/CitiesChart';

const Visualization = () => {
    return (
        <div className='center'>
            <ProductsChart />
            <RecipesChart />
            <LocationsChart />
            <CompaniesChart />
            <SectorsChart />
            <CitiesChart />
        </div>
    );
}

export default Visualization;
