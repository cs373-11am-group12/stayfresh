import React, { useEffect, useState } from 'react';
import axios from 'axios';
import RingLoader from 'react-spinners/RingLoader';
import {
    CardImg,
    Card,
    Row,
    Col,
} from 'react-bootstrap';
import {
    StringParam,
    NumberParam,
    useQueryParams,
    withDefault,
} from 'use-query-params';
import Pagination from '../../Pagination';
import SearchBar from '../../searchbar/SearchBar';
import Highlighter from "react-highlight-words";

const Products = () => {
    const endpoint = 'https://api.stay-fresh.me/products';
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(false);
    const [numProducts] = useState(10);

    const [params, setParams] = useQueryParams({
        page: withDefault(NumberParam, 1),
        q: withDefault(StringParam, ""),
        sort: withDefault(StringParam, ""),
        energy: withDefault(StringParam, ""),
        protein: withDefault(StringParam, ""), 
        fat: withDefault(StringParam, ""),
        carbs: withDefault(StringParam, ""),
        weight: withDefault(StringParam, ""), 
        fiber: withDefault(StringParam, "")
    });

    useEffect(() => {
        setLoading(true);
        let URLParams = new URLSearchParams();
        URLParams.append("q", params.q);
        if (params.sort)
            URLParams.append("sort", params.sort);
        if (params.energy)
            URLParams.append("energy", params.energy);
        if (params.protein)
            URLParams.append("protein", params.protein);
        if (params.fat)
            URLParams.append("fat", params.fat);
        if (params.carbs)
            URLParams.append("carbs", params.carbs);
        if (params.weight)
            URLParams.append("weight", params.weight);
        if (params.fiber)
            URLParams.append("fiber", params.fiber);
        console.log(`${endpoint}?${URLParams}`);
        axios
            .get(`${endpoint}?${URLParams}`)
            .then((response) => {
                setProducts(response.data['page']);
                setLoading(false);
            })
            .catch(e => console.log(e));
      }, [params]);

    const lastIndex = params.page * numProducts;
    const firstIndex = lastIndex - numProducts;
    const currentProducts = products.slice(firstIndex, lastIndex);

    const paginate = page => setParams({ ...params, page: page });

    const override = 
    `
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%)
    `;
    const Loading = () => 
        <RingLoader 
            color={'#7ED381'}
            loading={loading}
            css={override}
            size={150}
        />;

    const ShowProducts = () => {
        console.log(products);
        return (
            <>
                <Row xs={1} md={5} className="g-4">
                    {currentProducts.map(product => (
                        <Col key={product.id}>
                            <Card width = "20%" src={product}>
                                <Card.Link href={`/products/${product.id}`}>
                                <Card.Body>
                                    <h1><Highlighter autoEscape={true} textToHighlight={product.produce_name} searchWords={params.q.split(/[%20 \s,;/]+/)}/></h1>
                                    <CardImg width = "20%" src={product.produce_picture} alt="product image" />
                                    <Card.Title>Nurtition Facts</Card.Title>
                                    <Card.Text>
                                        Calories: {Number.parseFloat(product.produce_energy).toFixed(2)} kcal <br/>
                                        Protein: {Number.parseFloat(product.produce_protein).toFixed(2)} g<br/>
                                        Fat: {Number.parseFloat(product.produce_fat).toFixed(2)} g<br/>
                                        Carbs: {Number.parseFloat(product.produce_carbs).toFixed(2)} g<br/>
                                        Weight: {Number.parseFloat(product.produce_weight).toFixed(2)} g<br/>
                                        <br/>
                                    </Card.Text>
                                </Card.Body>
                                </Card.Link>
                            </Card>
                        </Col>
                    ))}
                </Row>
                <Pagination
                    instancesPer={numProducts}
                    totalInstances={products.length}
                    paginate={paginate}
                    page={params.page}
                />
            </>
        );
    }

    return (
        <>
            <SearchBar
                    filters={["Energy", "Protein", "Fat", "Carbs", "Weight", "Fiber"]}
                    filterOptions={[[], [], [], [], [], []]}
                    sorts={["Name (ascending)", "Name (descending)", "Energy (ascending)", "Energy (descending)", "Protein (ascending)", "Protein (descending)", "Fat (ascending)", "Fat (descending)", "Carbs (ascending)", "Carbs (descending)"]}
            />
            {loading ? <Loading /> : <ShowProducts />}
        </>
    );
}

export default Products;
