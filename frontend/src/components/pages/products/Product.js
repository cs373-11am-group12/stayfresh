import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import RingLoader from "react-spinners/RingLoader";
import { 
    Card,
    CardImg,
    CardGroup
} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';

const categories = {
    "fresh fruit and vegetables" : ["pea", "potato", "eggplant", "broccoli", "bell pepper", "onion", "tomato",
            "lentil", "chili pepper", "garlic", "ginger", "olive", "spinach", "mushroom", "kale",
            "basil", "kidney bean", "carrots", "celery", "jalapeno", "lettuce", "cucumber", "red onion",
            "yellow onion", "shallot", "avacado", "horseradish", "cabbage", "fava beans", "plum tomato",
            "chile", "beetroot", "radish", "bok choy", "bean sprout",
            "lemon", "orange", "mango", "apple", "plum", "blackberry", "raspberry", "lime", "coconut",
            "pear", "banana", "kiwi", "stawberry", "watermelon", "guava"],
    "Canned or preserved fruits, vegetables, jams, jellies, preserves, salsas, pickles, dried fruit, etc." : ["raisin"],
    "Fish and/or seafood" : ["mussels", "fish", "shrimp", "salmon", "anchovy", "prawn", "squid", "haddock", "cod"],
    "meat" : ["chicken", "sausage", "beef", "pork", "duck", "bacon", "ham", "prosciutto", "pancetta"],
    "Cheese and/or dairy products" : ["sour cream", "paneer", "milk", "cheese", "yogurt", "cheddar cheese", "gouda cheese", "mozzerlla cheese", "feta"],
    "honey" : ["honey"],
    "Wine, beer, hard cider" : ["wine"],
    "eggs" : ["egg"],
    "nuts" : ["walnut"],
    "Maple syrup and/or maple products" : ["maple syrup"],
    "herbs" : ["coriander", "cinnamon","mint" ,"thyme", "bay leaf", "cumin", "parsley", "rosemary", "clove",
        "saffron", "paprika", "dill"],
    "baked goods" : ["bread"],
    "Prepared foods (for immediate consumption)" : ["peanut butter", "mayonnaise", "mustard", "pasta", "fish stock", "chicken stock", "ketchup"]
};

const Product = () => {
    const { id } = useParams();
    const endpoint = 'https://api.stay-fresh.me/products';
    const [product, setProduct] = useState([]);
    const [category, setCategory] = useState("default");

    useEffect(() =>{
        axios
            .get(endpoint)
            .then(res => {
                setProduct(res.data['page'][parseInt(id) - 1]);
                let product_string = `${res.data['page'][parseInt(id) - 1].produce_name}`;
                for(var c in categories) {
                    let arr = categories[c];
                        if(arr.includes(product_string.toLowerCase())){
                            console.log(c);
                            setCategory(c);
                        }
                }
            })
            .catch(e => console.log(e));
    },[id]);

    const ShowProduct = () => {
        return (
            <div aria-label='product' style={{display: 'flex', justifyContent: 'center', padding: "3em 0em",}}>
                <Card className="text-center"style={{width:"70%"}}>
                    <Card.Header >Product</Card.Header>
                    <Card.Body>
                        <h1>{product.produce_name}</h1>
                        <CardImg width = "20%" src={product.produce_picture} alt="product image" />
                        <Card.Title>Nurtition Facts</Card.Title>
                        <Card.Text>
                                Calories: {product.produce_energy} kcal <br/>
                                Protein: {product.produce_protein} g<br/>
                                Fat: {product.produce_fat} g<br/>
                                Carbs: {product.produce_carbs} g<br/>
                                Weight: {product.produce_weight} g<br/>
                            <div style= {{display: 'flex', justifyContent: 'center', padding: "0em 3em",}}></div>
                        </Card.Text>
                    </Card.Body>
                <div style={{justifyContent: 'center', padding: "1em 1em",}}>
                <CardGroup>
                    <Card>
                        <Card.Header >Farmer's Markets</Card.Header>
                        <Card.Body>
                        <Card.Text>
                            <Button href={`/locations/?page=1&products=${category}`} variant="success">View Markets with {product.produce_name}</Button>
                        </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Header>Recipes</Card.Header>
                        <Card.Body>
                            <Button href={`/recipes/?ingredients=${product.produce_name}&page=1`} variant="success">View Recipes with {product.produce_name}</Button>
                        </Card.Body>
                    </Card>
                </CardGroup>
                </div>
            <Card.Footer className="text-muted"><Card.Link href="/products?page=1">View more products</Card.Link></Card.Footer>
            </Card>
        </div>
        );
    }

    return (<ShowProduct />);
}

export default Product;
