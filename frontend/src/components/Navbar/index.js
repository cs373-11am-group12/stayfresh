import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Container } from 'react-bootstrap';
import { Outlet, Link } from 'react-router-dom';

const NavBar = () => {
    return (
        <>
            <Navbar bg="success" variant="dark">
                <Container fluid>
                    <Container fluid>
                        <Navbar.Brand className="d-inline-block align-top" href='/' id = "nav-link-logo">
                            <img height="60" src={require ('./horizontal-logo.png')} alt="Stay Fresh" />
                        </Navbar.Brand>
                    </Container>
                    <Navbar.Toggle />
                    <Navbar.Collapse>
                        <Nav className="me-auto">
                            <Nav.Link as={Link} to="products?page=1">
                                Products
                            </Nav.Link>
                            <Nav.Link as={Link} to="recipes?page=1">
                                Recipes
                            </Nav.Link>
                            <Nav.Link as={Link} to="locations?page=1">
                                Locations
                            </Nav.Link>
                            <Nav.Link as={Link} to="about">
                                About
                            </Nav.Link>
                            <Nav.Link as={Link} to="search">
                                Search
                            </Nav.Link>
                            <Nav.Link as={Link} to="visualizations">
                                Visualizations
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Outlet />
        </>
    );
};

export default NavBar;