import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import reportWebVitals from './reportWebVitals';
import {
    BrowserRouter as Router,
    useNavigate,
    useLocation
} from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params'; 

const RouteAdapter = ({ children }) => {
    const navigate= useNavigate();
    const location = useLocation();

    const adaptedHistory = React.useMemo(
        () => ({
            replace(location) {
                navigate(location, { replace: true, state: location.state });
            },
            push(location) {
                navigate(location, { replace: false, state: location.state })
            },
        }),
        [navigate]
    );
    return children({ history: adaptedHistory, location });
};

ReactDOM.render(
  <Router>
      <QueryParamProvider ReactRouterRoute={RouteAdapter}>
        <App />
      </QueryParamProvider>
  </Router>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
