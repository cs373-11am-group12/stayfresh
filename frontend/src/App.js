import React from 'react';
import './App.css';
import NavBar from './components/Navbar';
import {
    BrowserRouter as Router, 
    Routes, 
    Route 
} from 'react-router-dom';
import Home from './components/pages/Home';
import About from './components/pages/About';
import Search from './components/pages/Search';
import Visualization from './components/pages/visualizations/Visualization';
import Products from './components/pages/products/Products';
import Product from './components/pages/products/Product';
import Recipes from './components/pages/recipes/Recipes';
import Recipe from './components/pages/recipes/Recipe'
import Locations from './components/pages/locations/Locations';
import Location from './components/pages/locations/Location';

const App = () => {
    const error = () => {return (<h1>Nothing here</h1>)};

    return (
        <>
            <NavBar />
            <Routes>
                <Route path="/" index element={<Home />} />
                    <Route path="/products" element={<Products />} />
                        <Route path="/products/:id" element={<Product />} />
                    <Route path="/recipes" element={<Recipes />} />
                        <Route path="/recipes/:id" element={<Recipe />} />
                    <Route path="/locations" element={<Locations />} />
                        <Route path="/locations/:id" element={<Location />} />
                    <Route path="/about" element={<About />} />
                    <Route path="/search" element={<Search />} />
                    <Route path="/visualizations" element={<Visualization />} />
                <Route path="*" element={error} />
            </Routes>
        </>
    );
}

export default App;
