import { screen, waitFor, toEqual, toBe,  } from '@testing-library/react';
import renderer, { act } from 'react-test-renderer';
import {render, unmountComponentAtNode} from 'react-dom'
import React from 'react'
import Location from './components/pages/locations/Location';
import Locations from './components/pages/locations/Locations';
import Product from './components/pages/products/Product';
import Products from './components/pages/products/Products';
import Recipe from './components/pages/recipes/Recipe';
import Recipes from './components/pages/recipes/Recipes';
import App from './App.js';
import NavBar from './components/Navbar/index';
import Home from './components/pages/Home';
import About from './components/pages/About';
import { Navbar, Pagination } from 'react-bootstrap';
import { CardImg } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import CardGroup from 'react-bootstrap/CardGroup';
import {
  BrowserRouter as Router, 
  Routes, 
  Route 
} from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';


// test 1
test('splash test', async () => {
  let component = renderer.create(<QueryParamProvider><Router><Home /></Router></QueryParamProvider>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
})

// test 2
test('navbar test', async () => {
  let component = renderer.create(<Router><NavBar /></Router>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
})

// test 3
test('card test', () => {
  let component = renderer.create(<Card />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
})

// // test 4
it('card text test', () => {
  let component = renderer.create(<Card.Text>Hello</Card.Text>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot(); 
})

// // test 5
test('card image test', () => {
  let component = renderer.create(<Card.Img />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot(); 
})


// // test 6
test('card title test', () => {
  let component = renderer.create(<Card.Title>Hello World</Card.Title>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot(); 
})

// // test 7
test('card footer test', () => {
  let component = renderer.create(<Card.Footer>Wonderful</Card.Footer>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot(); 
})

// // test 8
test('card body test', () => {
  let component = renderer.create(<Card.Body>Why</Card.Body>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot(); 
})

// // test 9
test('pagination test', () => {
  let component = renderer.create(<Pagination />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot(); 
})

// // test 10
test('card link test', () => {
  let component = renderer.create(<Card.Link></Card.Link>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot(); 
})