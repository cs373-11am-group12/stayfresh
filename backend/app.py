from flask import jsonify, request
from models import (
    Location,
    Produce,
    Recipe,
    app,
    db,
    LocationSchema,
    ProduceSchema,
    RecipeSchema,
)
from flask_cors import CORS, cross_origin
from query_helpers import object_as_dict
from filterRecipes import *
from filterLocations import *
from filterProduce import *

CORS(app)


@app.route("/")
def index():
    return "<h2>Welcome to stayfresh API!</h2>"


@app.route("/locations", methods=["GET"])
@cross_origin()
def location():
    queries = request.args.to_dict(flat=False)
    locations = db.session.query(Location)

    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        page = int(page[0])

    li = []

    # searching
    q = get_query("q", queries)
    if q:
        locations = search_locations(q, locations)

    # filtering
    locations = filter_locations(locations, queries)

    # sorting
    sort = get_query("sort", queries)
    locations = sort_locations(sort, locations)

    count = locations.count()

    if page != -1:
        per_page = (
            int(get_query("perPage", queries).pop())
            if get_query("perPage", queries)
            else 150
        )
        locations_q = locations.paginate(page=page, per_page=per_page)

        for l in locations_q.items:
            curr_dict = {
                "id": l.id,
                "location_name": l.location_name,
                "location_map": l.location_map,
                "location_products": l.location_products,
                "location_schedule": l.location_schedule,
                "location_address": l.location_address,
            }
            li.append(curr_dict)
    else:
        for l in locations:
            curr_dict = {
                "id": l.id,
                "location_name": l.location_name,
                "location_map": l.location_map,
                "location_products": l.location_products,
                "location_schedule": l.location_schedule,
                "location_address": l.location_address,
            }
            li.append(curr_dict)

    return jsonify({"page": li, "count": count})


@app.route("/locations/<id>", methods=["GET"])
@cross_origin()
def get_location_detail(id):
    location = Location.query.get(id)
    d = object_as_dict(location)
    return jsonify(d)


@app.route("/products", methods=["GET"])
@cross_origin()
def produce():
    queries = request.args.to_dict(flat=False)
    products = db.session.query(Produce)

    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        page = int(page[0])

    li = []

    # searching
    q = get_query("q", queries)
    if q:
        products = search_Produce(q, products)

    # filtering
    products = filter_produce(products, queries)

    # sorting
    sort = get_query("sort", queries)
    products = sort_produce(sort, products)

    count = products.count()

    if page != -1:
        per_page = (
            int(get_query("perPage", queries).pop())
            if get_query("perPage", queries)
            else 150
        )
        prod = products.paginate(page=page, per_page=per_page)

        for p in prod.items:
            curr_dict = {
                "id": p.id,
                "produce_name": p.produce_name,
                "produce_energy": p.produce_energy,
                "produce_protein": p.produce_protein,
                "produce_fat": p.produce_fat,
                "produce_carbs": p.produce_carbs,
                "produce_weight": p.produce_weight,
                "produce_fiber": p.produce_fiber,
                "produce_picture": p.produce_picture,
            }
            li.append(curr_dict)
    else:
        for p in products:
            curr_dict = {
                "id": p.id,
                "produce_name": p.produce_name,
                "produce_energy": p.produce_energy,
                "produce_protein": p.produce_protein,
                "produce_fat": p.produce_fat,
                "produce_carbs": p.produce_carbs,
                "produce_weight": p.produce_weight,
                "produce_fiber": p.produce_fiber,
                "produce_picture": p.produce_picture,
            }
            li.append(curr_dict)

    return jsonify({"page": li, "count": count})


@app.route("/products/<id>", methods=["GET"])
@cross_origin()
def get_produce_detail(id):
    produce = Produce.query.get(id)
    d = object_as_dict(produce)
    return jsonify(d)


@app.route("/recipes", methods=["GET"])
@cross_origin()
def recipe():
    queries = request.args.to_dict(flat=False)
    recipe_query = db.session.query(Recipe)

    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        page = int(page[0])
    li = []

    # searching
    q = get_query("q", queries)
    if q:
        recipe_query = search_recipes(q, recipe_query)

    # filtering
    recipe_query = filter_recipes(recipe_query, queries)

    # sorting
    sort = get_query("sort", queries)
    recipe_query = sort_recipes(sort, recipe_query)

    count = recipe_query.count()

    if page != -1:
        per_page = (
            int(get_query("perPage", queries).pop())
            if get_query("perPage", queries)
            else 150
        )
        recipes = recipe_query.paginate(page=page, per_page=per_page)

        for r in recipes.items:
            curr_dict = {
                "id": r.id,
                "recipe_name": r.recipe_name,
                "recipe_meal": r.recipe_meal,
                "recipe_type": r.recipe_type,
                "recipe_health": r.recipe_health,
                "recipe_cautions": r.recipe_cautions,
                "recipe_calories": r.recipe_calories,
                "recipe_fat": r.recipe_fat,
                "recipe_protein": r.recipe_protein,
                "recipe_sugar": r.recipe_sugar,
                "recipe_ingredients": r.recipe_ingredients,
                "recipe_instructions": r.recipe_instructions,
                "recipe_picture": r.recipe_picture,
            }
            li.append(curr_dict)
    else:

        for r in recipe_query:
            curr_dict = {
                "id": r.id,
                "recipe_name": r.recipe_name,
                "recipe_meal": r.recipe_meal,
                "recipe_type": r.recipe_type,
                "recipe_health": r.recipe_health,
                "recipe_cautions": r.recipe_cautions,
                "recipe_calories": r.recipe_calories,
                "recipe_fat": r.recipe_fat,
                "recipe_protein": r.recipe_protein,
                "recipe_sugar": r.recipe_sugar,
                "recipe_ingredients": r.recipe_ingredients,
                "recipe_instructions": r.recipe_instructions,
                "recipe_picture": r.recipe_picture,
            }
            li.append(curr_dict)

    return jsonify({"page": li, "count": count})


@app.route("/recipes/<id>", methods=["GET"])
@cross_origin()
def get_recipe_detail(id):
    recipe = Recipe.query.get(id)
    d = object_as_dict(recipe)
    return jsonify(d)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug="True", port=8000)
