import os
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://stayfresh:CS373StayFresh@stayfresh-database.cyhjrzkqiifo.us-east-1.rds.amazonaws.com:5432/postgres"

db = SQLAlchemy(app)
ma = Marshmallow(app)


# SCHEMAS #
class Produce(db.Model):
    __tablename__ = "produce"
    id = db.Column(db.Integer, primary_key=True)
    produce_name = db.Column(db.String())
    produce_energy = db.Column(db.Float())
    produce_protein = db.Column(db.Float())
    produce_fat = db.Column(db.Float())
    produce_carbs = db.Column(db.Float())
    produce_weight = db.Column(db.Float())
    produce_fiber = db.Column(db.Float())
    produce_picture = db.Column(db.String())

    def __init__(
        self,
        produce_name="NaN",
        produce_energy="NaN",
        produce_protein="NaN",
        produce_fat="NaN",
        produce_carbs="NaN",
        produce_weight="NaN",
        produce_fiber="NaN",
        produce_picture="NaN",
    ):
        self.produce_name = produce_name
        self.produce_energy = produce_energy
        self.produce_protein = produce_protein
        self.produce_fat = produce_fat
        self.produce_carbs = produce_carbs
        self.produce_weight = produce_weight
        self.produce_fiber = produce_fiber
        self.produce_picture = produce_picture


class Location(db.Model):
    __tablename__ = "location"
    id = db.Column(db.Integer, primary_key=True)
    location_name = db.Column(db.String())
    location_map = db.Column(db.String())
    location_products = db.Column(db.String())
    location_schedule = db.Column(db.String())
    location_address = db.Column(db.String())

    def __init__(
        self,
        location_name="NaN",
        location_map="NaN",
        location_products="NaN",
        location_schedule="NaN",
        location_address="NaN",
    ):
        self.location_name = location_name
        self.location_map = location_map
        self.location_products = location_products
        self.location_schedule = location_schedule
        self.location_address = location_address


class Recipe(db.Model):
    __tablename__ = "recipe"
    id = db.Column(db.Integer, primary_key=True)
    recipe_name = db.Column(db.String())
    recipe_meal = db.Column(db.String())
    recipe_type = db.Column(db.String())
    recipe_health = db.Column(db.String())
    recipe_cautions = db.Column(db.String())
    recipe_calories = db.Column(db.Float())
    recipe_fat = db.Column(db.Float())
    recipe_protein = db.Column(db.Float())
    recipe_sugar = db.Column(db.Float())
    recipe_ingredients = db.Column(db.String())
    recipe_instructions = db.Column(db.String())
    recipe_picture = db.Column(db.String())

    def __init__(
        self,
        recipe_name="NaN",
        recipe_meal="NaN",
        recipe_type="NaN",
        recipe_health="NaN",
        recipe_cautions="NaN",
        recipe_calories="NaN",
        recipe_fat="NaN",
        recipe_protein="NaN",
        recipe_sugar="NaN",
        recipe_ingredients="NaN",
        recipe_instructions="NaN",
        recipe_picture="NaN",
    ):
        self.recipe_name = recipe_name
        self.recipe_meal = recipe_meal
        self.recipe_type = recipe_type
        self.recipe_health = recipe_health
        self.recipe_cautions = recipe_cautions
        self.recipe_calories = recipe_calories
        self.recipe_fat = recipe_fat
        self.recipe_protein = recipe_protein
        self.recipe_sugar = recipe_sugar
        self.recipe_ingredients = recipe_ingredients
        self.recipe_instructions = recipe_instructions
        self.recipe_picture = recipe_picture


class ProduceSchema(ma.Schema):
    class Meta:
        fields = (
            "produce_id",
            "produce_name",
            "produce_energy",
            "produce_protein",
            "produce_fat",
            "produce_carbs",
            "produce_weight",
            "produce_fiber",
            "produce_picture",
        )


class LocationSchema(ma.Schema):
    class Meta:
        fields = (
            "location_id",
            "location_name",
            "location_map",
            "location_products",
            "location_schedule",
            "location_address",
        )


class RecipeSchema(ma.Schema):
    class Meta:
        fields = (
            "recipe_id",
            "recipe_name",
            "recipe_meal",
            "recipe_type",
            "recipe_health",
            "recipe_cautions",
            "recipe_calories",
            "recipe_fat",
            "recipe_protein",
            "recipe_sugar",
            "recipe_ingredients",
            "recipe_instructions",
            "recipe_picture",
        )
