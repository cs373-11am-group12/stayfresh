import os
import json
import flask
import requests
import sqlalchemy as db

recipes = set()
recipe_json = {}
recipe_json["recipe"] = []


def getRandomRecipes():
    request_url = "https://www.themealdb.com/api/json/v1/1/random.php"
    records = []
    count = 0
    with open("recipes.txt", "w") as data_file:
        for i in range(5000):
            response = requests.get(request_url).json()
            recipe_name = response["meals"][0]["strMeal"]
            if recipe_name not in recipes:
                count += 1
                recipes.add(recipe_name)
                new_recipe = {}
                new_recipe["name"] = recipe_name
                recipe_json["recipe"].append(new_recipe)
                data_file.write(recipe_name)
                data_file.write("\n")
    print(count)


if __name__ == "__main__":
    getRandomRecipes()
    print("Done")
