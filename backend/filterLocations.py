from sqlalchemy.sql.expression import null
from models import Location, db

from sqlalchemy import and_, or_, func
from query_helpers import *


def filter_locations_by(location_query, filtering, what):
    print(what[0])
    if filtering == "products":
        location_query = location_query.filter(
            func.lower(Location.location_products).contains(what[0])
        )
    elif filtering == "schedule":
        location_query = location_query.filter(
            func.lower(Location.location_schedule).contains(what[0])
        )
    elif filtering == "address":
        location_query = location_query.filter(
            func.lower(Location.location_address).contains(what[0])
        )
    elif filtering == "lzip":
        location_query = location_query.filter(
            Location.location_address.contains(what[0])
        )

    return location_query


def filter_locations(location_query, queries):
    products = get_query("products", queries)
    schedule = get_query("schedule", queries)
    address = get_query("address", queries)
    lzip = get_query("lzip", queries)

    if products != None:
        location_query = filter_locations_by(location_query, "products", products)
    if schedule != None:
        location_query = filter_locations_by(location_query, "schedule", schedule)
    if address != None:
        location_query = filter_locations_by(location_query, "address", address)
    if lzip != None:
        location_query = filter_locations_by(location_query, "lzip", lzip)

    return location_query


def sort_location_by(sorting, location_query, desc):
    location = None

    if sorting == "name":
        location = Location.location_name
    elif sorting == "schedule":
        location = Location.location_schedule
    elif sorting == "address":
        location = Location.location_address
    else:
        return location_query

    if desc:
        return location_query.order_by(location.desc())
    else:
        return location_query.order_by(location)


def sort_locations(sort, location_query):
    if sort == None:
        return location_query
    else:
        sort = sort[0]

    sort = sort.split("-")

    if len(sort) > 1:
        return sort_location_by(sort[1], location_query, True)
    else:
        return sort_location_by(sort[0], location_query, False)


def search_locations(q, location_query):
    if not q:
        return location_query
    else:
        q = q[0].strip()

    terms = q.split()

    searches = []
    for term in terms:
        searches.append(func.lower(Location.location_name).contains(func.lower(term)))
        searches.append(
            func.lower(Location.location_schedule).contains(func.lower(term))
        )
        searches.append(
            func.lower(Location.location_address).contains(func.lower(term))
        )
        searches.append(
            func.lower(Location.location_products).contains(func.lower(term))
        )

    print(location_query)

    location_query = location_query.filter(or_(*tuple(searches)))

    return location_query
