import requests
import json
from models import db, Location


def getZipCode():
    response = requests.get(
        "https://api.zip-codes.com/ZipCodesAPI.svc/1.0/GetAllZipCodes?state=CA&country=US&key=T8DCWQQRAVVSYSHBT7EF"
    )
    return response.json()


def getLocationID(zipcodes):
    count = 0
    index = 0
    ids = {}
    print("starting")
    while count < 150:
        print("before request")
        response = requests.get(
            "http://search.ams.usda.gov/farmersmarkets/v1/data.svc/zipSearch?zip="
            + zipcodes[index]
        )
        print("after request")
        data = response.json()
        print("in initial loop")
        for i in data["results"]:
            print("getting all zip codes")
            if i["id"] != "Error":
                ids[i["id"]] = i["marketname"]
                print(i["marketname"])
        count += len(data["results"])
        print(count)
        index += 1
    return ids


def getMarkets(ids):
    markets = {}
    keys = ids.keys()
    print("getting markets")
    for id in keys:
        response = requests.get(
            "http://search.ams.usda.gov/farmersmarkets/v1/data.svc/mktDetail?id=" + id
        )
        data = response.json()["marketdetails"]
        markets[ids[id]] = data
    return markets


def getData():
    zipcodes = getZipCode()
    ids = getLocationID(zipcodes)
    markets = getMarkets(ids)
    return markets


if __name__ == "__main__":
    marketDetails = getData()
    db.create_all()
    marketNames = marketDetails.keys()
    markets = []
    print("making db stuff")
    for key in marketNames:
        market = Location(
            location_name=key,
            location_map=marketDetails[key]["GoogleLink"],
            location_products=marketDetails[key]["Products"],
            location_schedule=marketDetails[key]["Schedule"],
            location_address=marketDetails[key]["Address"],
        )
        markets.append(market)
    db.session.add_all(markets)
    db.session.commit()
    print("done")
