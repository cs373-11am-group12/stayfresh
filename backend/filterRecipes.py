from sqlalchemy.sql.expression import null
from models import Recipe, db

from sqlalchemy import and_, or_, func
from query_helpers import *


def filter_recipes_by(recipe_query, filtering, what):
    if filtering == "meal":
        recipe_query = recipe_query.filter(func.lower(Recipe.recipe_meal).in_(what))
    elif filtering == "rtype":
        recipe_query = recipe_query.filter(func.lower(Recipe.recipe_type).in_(what))
    elif filtering == "health":
        recipe_query = recipe_query.filter(
            func.lower(Recipe.recipe_health).contains(what[0])
        )
    elif filtering == "cautions":
        recipe_query = recipe_query.filter(
            func.lower(Recipe.recipe_cautions).contains(what[0])
        )
    elif filtering == "ingredients":
        recipe_query = recipe_query.filter(
            func.lower(Recipe.recipe_ingredients).contains(what[0])
        )

    return recipe_query


def filter_recipes(recipe_query, queries):
    meal = get_query("meal", queries)
    rtype = get_query("rtype", queries)
    health = get_query("health", queries)
    cautions = get_query("cautions", queries)
    ingredients = get_query("ingredients", queries)

    if meal != None:
        recipe_query = filter_recipes_by(recipe_query, "meal", meal)
    if rtype != None:
        recipe_query = filter_recipes_by(recipe_query, "rtype", rtype)
    if health != None:
        recipe_query = filter_recipes_by(recipe_query, "health", health)
    if cautions != None:
        recipe_query = filter_recipes_by(recipe_query, "cautions", cautions)
    if ingredients != None:
        recipe_query = filter_recipes_by(recipe_query, "ingredients", ingredients)

    return recipe_query


def sort_recipe_by(sorting, recipe_query, desc):
    recipe = None

    if sorting == "name":
        recipe = Recipe.recipe_name
    elif sorting == "meal":
        recipe = Recipe.recipe_meal
    elif sorting == "type":
        recipe = Recipe.recipe_type
    elif sorting == "health":
        recipe = Recipe.recipe_health
    elif sorting == "cautions":
        recipe = Recipe.recipe_cautions
    else:
        return recipe_query

    if desc:
        return recipe_query.order_by(recipe.desc())
    else:
        return recipe_query.order_by(recipe)


def sort_recipes(sort, recipe_query):
    if sort == None:
        return recipe_query
    else:
        sort = sort[0]

    sort = sort.split("-")

    if len(sort) > 1:
        return sort_recipe_by(sort[1], recipe_query, True)
    else:
        return sort_recipe_by(sort[0], recipe_query, False)


def search_recipes(q, recipe_query):
    if not q:
        return recipe_query
    else:
        q = q[0].strip()

    terms = q.split()

    searches = []
    for term in terms:
        searches.append(func.lower(Recipe.recipe_name).contains(func.lower(term)))
        searches.append(func.lower(Recipe.recipe_meal).contains(func.lower(term)))
        searches.append(func.lower(Recipe.recipe_type).contains(func.lower(term)))
        searches.append(func.lower(Recipe.recipe_health).contains(func.lower(term)))
        searches.append(func.lower(Recipe.recipe_cautions).contains(func.lower(term)))

    recipe_query = recipe_query.filter(or_(*tuple(searches)))

    return recipe_query
