import json
import requests
from models import db, Recipe


def getRecipe(recipe):
    recipe_name = recipe.replace(" ", "%20")
    api_url = (
        "https://api.edamam.com/api/recipes/v2?type=public&random=true&app_id=183a0ef1&app_key=41663171c2abb8782209018eb66d7f9d&q="
        + recipe_name
    )
    try:
        response = requests.get(api_url)
        json_data = response.json()
        new_recipe = Recipe(
            recipe_name=json_data["hits"][0]["recipe"]["label"],
            recipe_meal=json_data["hits"][0]["recipe"]["mealType"][0],
            recipe_type=json_data["hits"][0]["recipe"]["cuisineType"][0],
            recipe_health=",".join(json_data["hits"][0]["recipe"]["healthLabels"]),
            recipe_cautions=",".join(json_data["hits"][0]["recipe"]["cautions"]),
            recipe_calories=json_data["hits"][0]["recipe"]["totalNutrients"][
                "ENERC_KCAL"
            ]["quantity"],
            recipe_fat=json_data["hits"][0]["recipe"]["totalNutrients"]["FAT"][
                "quantity"
            ],
            recipe_protein=json_data["hits"][0]["recipe"]["totalNutrients"]["PROCNT"][
                "quantity"
            ],
            recipe_sugar=json_data["hits"][0]["recipe"]["totalNutrients"]["SUGAR"][
                "quantity"
            ],
            recipe_ingredients=",".join(
                json_data["hits"][0]["recipe"]["ingredientLines"]
            ),
            recipe_instructions=json_data["hits"][0]["recipe"]["url"],
            recipe_picture=json_data["hits"][0]["recipe"]["image"],
        )
        return new_recipe
    except:
        return "NaN"


if __name__ == "__main__":
    recipes = []
    # for recipe in #recipe list
    db.create_all()
    print("putting recipes in")
    with open("recipes.txt") as f:
        recipe = f.readline()
        while recipe:
            print(recipe)
            new_recipe = getRecipe(recipe)
            if new_recipe != "NaN":
                recipes.append(new_recipe)
            recipe = f.readline()
    f.close()
    db.session.add_all(recipes)
    db.session.commit()
    print("done")
