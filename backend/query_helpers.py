from sqlalchemy import inspect


def get_query(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None


def object_as_dict(obj):
    return {c.key: getattr(obj, c.key) for c in inspect(obj).mapper.column_attrs}
