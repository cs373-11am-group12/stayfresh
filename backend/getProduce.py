import json
import requests
from models import db, Produce


def getProduce(produce_name):
    produce_name = produce_name.replace(" ", "%20")
    api_url = (
        "https://api.edamam.com/api/food-database/v2/parser?app_id=cf9efebc&app_key=e56e86e65ff6702a4acb3ee0d2a19235&ingr="
        + produce_name
    )
    try:
        response = requests.get(api_url)
        json_data = response.json()
        new_produce = Produce(
            produce_name=json_data["parsed"][0]["food"]["label"],
            produce_energy=json_data["parsed"][0]["food"]["nutrients"]["ENERC_KCAL"],
            produce_protein=json_data["parsed"][0]["food"]["nutrients"]["PROCNT"],
            produce_fat=json_data["hints"][0]["food"]["nutrients"]["FAT"],
            produce_carbs=json_data["hints"][0]["food"]["nutrients"]["CHOCDF"],
            produce_weight=json_data["hints"][0]["measures"][0]["weight"],
            produce_fiber=json_data["hints"][0]["food"]["nutrients"]["FIBTG"],
            produce_picture=json_data["hints"][0]["food"]["image"],
        )
        return new_produce
    except:
        return "NaN"


if __name__ == "__main__":
    produce = []
    # for recipe in #recipe list
    db.create_all()
    print("Begin reading produce file.")
    with open("produce.txt") as f:
        product = f.readline()
        while product:
            print("Add to db: " + str(product))
            new_produce = getProduce(product)
            if new_produce != "NaN":
                produce.append(new_produce)
            product = f.readline()
    f.close()
    db.session.add_all(produce)
    db.session.commit()
    print("Done putting produce in db!")
