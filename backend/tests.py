import unittest
from app import app


class UnitTest(unittest.TestCase):
    def test_home_page(self):
        test_client = app.test_client(self)
        response = test_client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_retrieve_locations(self):
        test_client = app.test_client(self)
        response = test_client.get("/locations")
        self.assertEqual(response.status_code, 200)

    def test_retrieve_locations_id(self):
        test_client = app.test_client(self)
        response = test_client.get("/locations/3")
        self.assertEqual(response.status_code, 200)

    def test_retrieve_products(self):
        test_client = app.test_client(self)
        response = test_client.get("/products")
        self.assertEqual(response.status_code, 200)

    def test_retrieve_products_id(self):
        test_client = app.test_client(self)
        response = test_client.get("/products/5")
        self.assertEqual(response.status_code, 200)

    def test_retrieve_recipes(self):
        test_client = app.test_client(self)
        response = test_client.get("/recipes")
        self.assertEqual(response.status_code, 200)

    def test_retrieve_recipes_id(self):
        test_client = app.test_client(self)
        response = test_client.get("/recipes/7")
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
