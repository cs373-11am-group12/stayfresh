from sqlalchemy.sql.expression import null
from models import Produce, db

from sqlalchemy import and_, or_, func
from query_helpers import *


def filter_produce_by(produce_query, filtering, what):
    if filtering == "energy":
        produce_query = produce_query.filter(
            and_(
                (Produce.produce_energy) >= what[0],
                (Produce.produce_energy) <= what[1],
            )
        )
    elif filtering == "protein":
        produce_query = produce_query.filter(
            and_(
                (Produce.produce_protein) >= what[0],
                (Produce.produce_protein) <= what[1],
            )
        )
    elif filtering == "fat":
        produce_query = produce_query.filter(
            and_(
                (Produce.produce_fat) >= what[0],
                (Produce.produce_fat) <= what[1],
            )
        )
    elif filtering == "carbs":
        produce_query = produce_query.filter(
            and_(
                (Produce.produce_carbs) >= what[0],
                (Produce.produce_carbs) <= what[1],
            )
        )
    elif filtering == "weight":
        produce_query = produce_query.filter(
            and_(
                (Produce.produce_weight) >= what[0],
                (Produce.produce_weight) <= what[1],
            )
        )
    elif filtering == "fiber":
        produce_query = produce_query.filter(
            and_(
                (Produce.produce_fiber) >= what[0],
                (Produce.produce_fiber) <= what[1],
            )
        )

    return produce_query


def filter_produce(produce_query, queries):
    energy = get_query("energy", queries)
    protein = get_query("protein", queries)
    fat = get_query("fat", queries)
    carbs = get_query("carbs", queries)
    weight = get_query("weight", queries)
    fiber = get_query("fiber", queries)

    if energy != None:
        energy = energy[0]
        minEnergy = 0
        maxEnergy = 99999
        if len(energy.split("-")) < 2:
            maxEnergy = energy
        else:
            minEnergy, maxEnergy = energy.split("-")
        print(minEnergy)
        print(maxEnergy)
        produce_query = filter_produce_by(
            produce_query, "energy", [minEnergy, maxEnergy]
        )
    if protein != None:
        protein = protein[0]
        minProtein = 0
        maxProtein = 99999
        if len(protein.split("-")) < 2:
            maxProtein = protein
        else:
            minProtein, maxProtein = protein.split("-")
        produce_query = filter_produce_by(
            produce_query, "protein", [minProtein, maxProtein]
        )
    if fat != None:
        fat = fat[0]
        minFat = 0
        maxFat = 99999
        if len(fat.split("-")) < 2:
            maxFat = fat
        else:
            minFat, maxFat = fat.split("-")
        produce_query = filter_produce_by(produce_query, "fat", [minFat, maxFat])
    if carbs != None:
        carbs = carbs[0]
        minCarbs = 0
        maxCarbs = 99999
        if len(carbs.split("-")) < 2:
            maxCarbs = carbs
        else:
            minCarbs, maxCarbs = carbs.split("-")
        produce_query = filter_produce_by(produce_query, "carbs", [minCarbs, maxCarbs])
    if weight != None:
        weight = weight[0]
        minWeight = 0
        maxWeight = 99999
        if len(weight.split("-")) < 2:
            maxWeight = weight
        else:
            minWeight, maxWeight = weight.split("-")
        produce_query = filter_produce_by(
            produce_query, "weight", [minWeight, maxWeight]
        )
    if fiber != None:
        fiber = fiber[0]
        minFiber = 0
        maxFiber = 99999
        if len(fiber.split("-")) < 2:
            maxFiber = fiber
        else:
            minFiber, maxFiber = fiber.split("-")
        produce_query = filter_produce_by(produce_query, "fiber", [minFiber, maxFiber])

    return produce_query


def sort_produce_by(sorting, produce_query, desc):
    produce = None

    if sorting == "name":
        produce = Produce.produce_name
    elif sorting == "energy":
        produce = Produce.produce_energy
    elif sorting == "protein":
        produce = Produce.produce_protein
    elif sorting == "fat":
        produce = Produce.produce_fat
    elif sorting == "carbs":
        produce = Produce.produce_carbs
    else:
        return produce_query

    if desc:
        return produce_query.order_by(produce.desc())
    else:
        return produce_query.order_by(produce)


def sort_produce(sort, produce_query):
    if sort == None:
        return produce_query
    else:
        sort = sort[0]

    sort = sort.split("-")

    if len(sort) > 1:
        return sort_produce_by(sort[1], produce_query, True)
    else:
        return sort_produce_by(sort[0], produce_query, False)


def search_Produce(q, produce_query):
    if not q:
        return produce_query
    else:
        q = q[0].strip()

    terms = q.split()

    searches = []
    for term in terms:
        searches.append(func.lower(Produce.produce_name).contains(func.lower(term)))

    produce_query = produce_query.filter(or_(*tuple(searches)))

    return produce_query
