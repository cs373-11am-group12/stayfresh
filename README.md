Team members (EID, GitLab ID): 
- Maria Gu: mgu1115, @mgu1115
- William Gunawan: wg5232, @wgunawan2000
- Sanchith Shanmuga: ss89527, @sanchiths
- Francisco Villegas: , @franky52
- Ziyi Zhao: , @zhaozyi

Git SHA Phase 1: 6db73af1addd933efd76bf378da27fa38987f31a

Git SHA Phase 2: 29e046aefb0731ca409c9ad81d52d141381f9109

Git SHA Phase 3: e3380ad9f3da4ad13bf0ee414fa98897a87244d6

Git SHA Phase 4: 3fc6c14c448d08fbb63f49eb6e13deb1a5f8e64a

Project Leader Phase 1: Francisco Villegas

Project Leader Phase 2: William Gunawan

Project Leader Phase 3: Maria Gu

Project Leader Phase 4: Ziyi Zhao

Pipeline: https://gitlab.com/cs373-11am-group12/stayfresh/-/pipelines

Website: https://www.stay-fresh.me/

Api: https://api.stay-fresh.me/

YouTube Presentation: https://youtu.be/Xfcb4DUv0PU

Phase 1:

Estimated completion time for each member:
- Maria: 10 hrs
- William: 10 hrs
- Sanchith: 10 hrs
- Francisco: 10 hrs
- Ziyi: 10 hrs

Actual completion time for each member:
- Maria: 15 hrs
- William: 12 hrs
- Sanchith: 12 hrs
- Francisco: 15 hrs
- Ziyi: 15 hrs

Phase 2:

Estimated completion time for each member:
- Maria: 20 hrs
- William: 20 hrs
- Sanchith: 20 hrs
- Francisco: 20 hrs
- Ziyi: 20 hrs

Actual completion time for each member:
- Maria: 24 hrs
- William: 30 hrs
- Sanchith: 21 hrs
- Francisco: 22 hrs
- Ziyi: 23 hrs

Phase 3:

Estimated completion time for each member:
- Maria: 20 hrs
- William: 20 hrs
- Sanchith: 20 hrs
- Francisco: 20 hrs
- Ziyi: 20 hrs

Actual completion time for each member:
- Maria: 24 hrs
- William: 24 hrs
- Sanchith: 21 hrs
- Francisco: 22 hrs
- Ziyi: 23 hrs

Phase 4:

Estimated completion time for each member:
- Maria: 10 hrs
- William: 10 hrs
- Sanchith: 10 hrs
- Francisco: 10 hrs
- Ziyi: 10 hrs

Actual completion time for each member:
- Maria: 15 hrs
- William: 12 hrs
- Sanchith: 12 hrs
- Francisco: 15 hrs
- Ziyi: 15 hrs

Comments:
